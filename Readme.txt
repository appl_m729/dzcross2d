Directory struture

./settings -- the compilation options should be set in ./setting/Makefile.in
./src      -- the cross source code and the maxvol fortran code with its c wrapper
./include  -- common utils
./libs     -- libtcross2d.a will be placed here after the compilation
./examples -- examples
./docs     -- documentation in latex


Dependencies (./include)

tcross_2d --> target_func_2d --> defs_cross_2d
   |                         --> verbose_support --> uncopyable
   |                            ^                        
   |                       /----|                        
   |                      /                              
   |                     /                               
   |                    /                                
   |----->    tmatrix_utils  --> utils_zlapack --> defs_cross_2d
                             --> utils_dlapack
                                          
Make options:

./settings/Makefile.in --> CPU = {gnu, mac, intel}

General options:

all      -- assemble both library and examples
lib      -- assemle just libtcross2s.a
clean    -- clean *.o files
cleanall -- clean all the object files and the libtcross2d.a
