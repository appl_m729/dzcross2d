#!/bin/bash

for m in 10 100 1000 10000
do
    for n in 10 100 1000 10000
    do 
        for eps in 1e-4 1e-5 1e-6 1e-7 1e-8
        do
            OUTPUT="gnu_performance_"$m"_"$n"_"$eps".log"
            srun -N 1 -c 4 -p new ./a.out $n $m $eps >$OUTPUT &
        done
    done
done
