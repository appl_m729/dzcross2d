/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */
#include <iostream>
#include <cmath>
#include <iomanip>
#include <memory>
#include <complex>
#include "tmatrix_utils.h"
#include "tcross_2d.h"
#include "target_func_2d.h"
#include "verbose_support.h"
#include "defs_cross_2d.h"
#include <sys/time.h>

// #include "omp.h"

using  std::cout;
using  std::endl;
using  std::setw;
using  std::setprecision;
using  std::unique_ptr;
using  std::complex;
using  std::conj;
using  std::abs;

using  mulr_2d::TargetFunc2D;
using  mulr_2d::RectTargetFunc2D;
using  mulr_2d::BaseCross2D;
using  mulr_2d::StableCross2D;
using  mulr_2d::SVDCross2D;
using  mulr_2d::dcomplex;

///////////////////////////////////////////////////////////////////////////////

//class StableCross2D;

//template <typename T>
//bool test_svd_cross(BaseCross2D<T> * const cross2d, const int n);


int main(int argc, char * argv[]) {

    // Output detalization
    Verbose::DebugMemory::setShowFlag(false);
    Verbose::UserInform::setShowFlag(false);
    Verbose::TechnicalDetails::setShowFlag(false);

    /*const*/ int n = 16;
    /*const*/ int m = 16;
    /*const*/ double eps = 1e-4;
/*
    if (argc == 4)
    {
        n   = atoi(argv[1]);
        m   = atoi(argv[2]);
        eps = atof(argv[3]);
        std::cout << "Setup parameters" << std::endl;
        std::cout << "n = "   << n << std::endl;
        std::cout << "m = "   << m << std::endl;
        std::cout << "eps = " << eps << std::endl;
    }
    else
    {
        std::cout << "Default parameters" << std::endl;
        std::cout << "n = "   << n << std::endl;
        std::cout << "m = "   << n << std::endl;
        std::cout << "eps = " << eps << std::endl;
        std::cout << "To initialize, please, call like" << std::endl;
        std::cout << "./a.out 100 10 1e-4" << std::endl;
    }
*/
    RectTargetFunc2D<double> * rtf = new RectTargetFunc2D<double>();
//    RectTargetFunc2D<dcomplex> * rtf = new RectTargetFunc2D<dcomplex>();
    struct timeval start, end;
    //RectTargetFunc2D<dcomplex> * rtf = new RectTargetFunc2D<dcomplex>();

    rtf->set_nx_rows(n);
    rtf->set_my_cols(m);

    const double dx = 1e-3;
    rtf->set_ax_top(1.0 );
    rtf->set_bx_bottom(1.0 + dx);
    rtf->set_ay_left(1.0);
    rtf->set_by_right(1.0 + dx);

    if (!rtf->init()) {
        cout << "Error in target function setup. Check parameters!" << endl;
        return 0;
    }


//    for (int i = 0; i < rtf->nx_rows(); ++i) {
//        for (int j = 0; j < rtf->my_cols(); ++j) {
//            cout << setw(6) << i << setw(6) << j << setw(16) << rtf->value(i,j) << endl;
//        }
//        cout << endl;
//    }

    BaseCross2D<double> * const cross2d = new StableCross2D<double>();
//    BaseCross2D<double> * const cross2d = new SVDCross2D<double>();
    //BaseCross2D<dcomplex> * const cross2d = new StableCross2D<dcomplex>();

    cross2d->set_iter_limit(1024);
    cross2d->set_truncation_accuracy(eps);

    cross2d->set_target_function(rtf);  // ! must be set
    cross2d->set_guess_rank(4);         // ! must be set

//    gettimeofday(&start, NULL);	
    if (!cross2d->do_decompose()) {
        cout << " Error in XCross2D<T> decomposition!" << endl;
    }

    cout << " Singular values" << endl;
    for (int k = 0; k < cross2d->rank(); ++k) {
        cout << setw(5) << (k+1) << setw(30) << setprecision(16) << cross2d->Sigma(k) << endl;
    }
    cout << " rank = " << cross2d->rank() << endl;

//    gettimeofday(&end, NULL);
//    double r_time = end.tv_sec - start.tv_sec + ((double)(end.tv_usec - start.tv_usec)) / 1000000;
 //   cout << "Full time = " << r_time << " seconds" << endl;
//    const int i0 = 5000;
//    const int j0 = 5000;
//    const dcomplex aprx_val_ij = cross2d->approx_value(i0,j0);
//    const dcomplex func_val_ij = rtf->value(i0,j0);

//    cout << setw(32) << setprecision(17) << aprx_val_ij << endl;
//    cout << setw(32) << setprecision(17) << func_val_ij << endl;






//    double diff_sum2 = 0;
//    double norm_sum2 = 0;
//    for (int i = 0; i < n; ++i) {
//        for (int j = 0; j < m; ++j) {

//            const dcomplex aprx_val_ij = cross2d->approx_value(i,j);
//            const dcomplex func_val_ij = rtf->value(i,j);
//            const double delta_af_ij = abs(func_val_ij - aprx_val_ij);

//            diff_sum2 += fabs(delta_af_ij) * fabs(delta_af_ij);
//            norm_sum2 += fabs(func_val_ij) * fabs(func_val_ij);
//        }
//    }

//    cout << diff_sum2 << endl;
//    cout << norm_sum2 << endl;

//    const double approx_error = sqrt(diff_sum2 / norm_sum2);

    cout << "[" << rtf->ax_top() << "," << rtf->bx_bottom() << "] x [";
    cout << rtf->ay_left() << "," << rtf->by_right() << "]"<< endl;
    cout << "n = " << rtf->nx_rows() << "  m = " << rtf->my_cols() << endl;
    cout << "eps_cross = " << cross2d->accuracy() << endl;
/*
    double *x = (double *) malloc (n * sizeof(double));
    //dcomplex *x = (dcomplex *) malloc (n * sizeof(dcomplex));
    for (int k = 0; k < n; k++)
        x[k] = 1.0;
    double *y;
    //dcomplex *y;
    char opt = 'q';
    y = cross2d->matvec(x, opt);
    for (int k = 0; k < n; k++)
        printf("%lf \n", y[k]);
    cout << "matvec is done" << endl;
*/
//    cout << "Test passed with " << setw(11) << num_max_err << " K_max errors" << endl;
//    cout << "                 " << setw(11) << num_min_err << " K_min errors" << endl;

//    cout << "Max relative error = "<< setprecision(17) << setw(24) << err_max;
//    cout << " against " << eps << endl;
//    cout << "cross value in max err = ";
//    cout << std::setprecision(17) << cross2d->approx_value(i_err_max, j_err_max) << endl;
//    cout << "    f value in max err = ";
//    cout << std::setprecision(17) << rtf->value(i_err_max, j_err_max) << endl;
//    cout << std::setprecision(17) << "f maximum = " << f_val_max << endl;
//    cout << "cross max = " << cross2d->approx_value(i_val_max, j_val_max) << endl;

//    cout << "Min relative error = "<< setprecision(17) << setw(24) << err_min;
//    cout << " against " << eps << endl;
//    cout << "cross value in min err = ";
//    cout << std::setprecision(17) << cross2d->approx_value(i_err_min, j_err_min) << endl;
//    cout << "    f value in min err = ";
//    cout << std::setprecision(17) << rtf->value(i_err_min, j_err_min) << endl;
//    cout << std::setprecision(17) << "f minimum = " << f_val_min << endl;
//    cout << "cross min = " << cross2d->approx_value(i_val_min, j_val_min) << endl;

//    for (int i = 0; i < rtf->nx_rows(); ++i) {
//        for (int j = 0; j < rtf->my_cols(); ++j) {
//            cout << setw(6) << i << setw(6) << j << setw(26) << cross2d->approx_value(i,j);
//            cout << setw(26) << rtf->value(i,j) << endl;
//        }
//        cout << endl;
//    }

//    cout << " Relative error of the approximation: " << setw(30) << approx_error << endl;


    delete cross2d;
    delete rtf;

    return 0;
}


//    BaseCross2D<double> * const cross2d = new StableCross2D<double>();
//    BaseCross2D<double> * const cross2d = new SVDCross2D<double>();
//    BaseCross2D<dcomplex> * const cross2d = new StableCross2D<dcomplex>();
//    BaseCross2D<dcomplex> * const cross2d = new SVDCross2D<dcomplex>();

//    cross2d->set_target_function(rtf);
//    cross2d->set_truncation_accuracy(1e-10);
//    cross2d->set_guess_rank(3);
//    cross2d->set_iter_limit(1024);
//    if (!cross2d->do_decompose()) {
//        cout << " Error in X_Cross2D<T> decomposition!" << endl;
//    }

//    cout << " Singular values" << endl;
//    for (int k = 0; k < cross2d->rank(); ++k) {
//        cout << setw(5) << (k+1) << setw(30) << setprecision(16) << cross2d->Sigma(k) << endl;
//    }



////    if (!test_svd_cross<double>(cross2d, 6)) {
//    if (!test_svd_cross<dcomplex>(cross2d, 6)) {
//        cout << "BAD TEST CROSS" << endl;
//    }
//    cross2d->release_auxiliary_memory();


//    cout << endl << " _________ " << endl;
//    rtf->set_nx_rows(20);
//    rtf->set_my_cols(20);
//    rtf->init();
////    if (!test_svd_cross<double>(cross2d, 20)) {
//    if (!test_svd_cross<dcomplex>(cross2d, 20)) {
//        cout << "BAD TEST CROSS" << endl;
//    }
//    cross2d->release_auxiliary_memory();
//    cout << " Singular values" << endl;
//    for (int k = 0; k < cross2d->rank(); ++k) {
//        cout << setw(5) << (k+1) << setw(30) << setprecision(16) << cross2d->Sigma(k) << endl;
//    }


//    RectTargetFunc2D<dcomplex> * rtf2 = new RectTargetFunc2D<dcomplex>();
//    rtf2->set_ax_top(-2);
//    rtf2->set_bx_bottom(4);
//    rtf2->set_ay_left(-5);
//    rtf2->set_by_right(10);

//    rtf2->set_nx_rows(500);
//    rtf2->set_my_cols(500);
//    rtf2->init();


//    cout << " start new cross" << endl << endl;
//    cross2d->set_target_function(rtf2);
//    cross2d->set_truncation_accuracy(1e-13);
//    cross2d->set_guess_rank(3);
//    cross2d->set_iter_limit(1024);
//    if (!cross2d->do_decompose()) {
//        cout << " Error in X_Cross2D<T> decomposition 2!" << endl;
//    }


//    for (int i = 0; i < rtf->nx_rows(); ++i) {
//        for (int j = 0; j < rtf->my_cols(); ++j) {
//            const dcomplex av(cross2d->approx_value(i,j));
//            cout << av << "  " << (av - rtf->value(i,j)) << endl;
//        }
//    }

//--------------------------------------------
//    const T * adress_U() const;
//    const T * adress_Vh() const;
//    double accuracy() const;
//    T approx_value(const int i, const int j) const;
//    T  U(const int i, const int k) const;
//    T  Sigma(const int k) const;
//    T  Vh(const int k, const int j) const; // h - Hermitian conj
//    int rank() const;
//--------------------------------------------

//    dcross2d_svd->release_base_memory();
//    dcross2d_svd->release_auxiliary_memory();

//    if (!cross2d->do_decompose()) {
//        cout << " Error in ExtContrCross2D<T> decomposition." << endl;
//    }
//    dcross2d_svd->release_auxiliary_memory();

//    cout << "  ------------------ >>>>>  " << endl;
//    for (int i = 0; i < dcross2d_svd->nx_rows(); ++i) {
//        for (int j = 0; j < dcross2d_svd->my_cols(); ++j) {

//            const double diff_ij = dcross2d_svd->approx_value(i,j); // - rtf->value(i,j);

//            double sum_ij = zero_init<double>();
//            for (int k = 0; k < dcross2d_svd->rank(); ++k) {
//                sum_ij += dcross2d_svd->U(i,k) * dcross2d_svd->Sigma(k) * dcross2d_svd->Vh(k,j);
//            }

//            cout << setw(16) << diff_ij - sum_ij;
//        }
//        cout << endl;
//    }


//    delete rtf2;

//    return 0;
//}

//template <typename T>
//bool test_svd_cross(BaseCross2D<T> * const cross2d, const int n) {

//    T U_rnd[1000];
//    T V_rnd[1000];
////    for (int i = 0; i < n; ++i) {
////        for (int j = 0; j < n; ++j) {
////            U_rnd[i + j * n] =  1 + i + j * pow(10, j);
////            V_rnd[i + j * n] = -2 - i - j * pow(10, j);
////        }
////    }

//    random_fill_in<T>(U_rnd, n, n-2);

//    if (!cross2d->set_guess_rank(n-2)) {return false;}
//    if (!cross2d->set_guess_matrix_U(U_rnd)) {return false;}
////    cross2d->set_guess_matrix_V(V_rnd);

//    cross2d->set_truncation_accuracy(1e-10);
//    cross2d->set_iter_limit(1024);

//    if (!cross2d->do_decompose()) {
//        cout << " Error in X_Cross2D<T> decomposition." << endl;
//        return false;
//    }

//    cout << " Rank = " << cross2d->rank() << endl;
//    return true;
//}

///////////////////////////////////////////////////////////////////////////////

// End of the file

