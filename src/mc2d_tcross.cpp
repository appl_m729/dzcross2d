/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#include <algorithm>
#include <iostream>
#include <iomanip>
#include "mc2d_tcross.h"
#include "mc2d_tmatrix_utils.h"
#include "mc2d_target_func.h"
#include "mc2d_utils_dlapack.h"
#include "mc2d_utils_zlapack.h"
#include "mc2d_verbose_support.h"

using  std::cout;
using  std::endl;
using  std::setw;
using  std::conj;
using  std::min;
using  matrix_cross_2d::conj;

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

///---------------------------  BaseCross2D<T>  ---------------------------////
template <typename T>
BaseCross2D<T>::BaseCross2D():
    generator_fm_(nullptr),
    is_guess_rank_setup_(false),
    rank_(0),
    guess_rank_(4),
    rank_trancation_epsilon_(1e-4),
    Ur_(nullptr),
    Vr_(nullptr),
    up_Ur_(Ur_),
    up_Vr_(Vr_),
    Sigma_(nullptr),
    up_Sigma_(Sigma_) {
}

template <typename T>
BaseCross2D<T>::~BaseCross2D() {
    generator_fm_ = nullptr;
}

//virtual
template <typename T>
void BaseCross2D<T>::release_base_memory() {

    up_Ur_.reset(nullptr);
    up_Vr_.reset(nullptr);
    up_Sigma_.reset(nullptr);

    rank_ =  0;

    Ur_    = nullptr;
    Vr_    = nullptr;
    Sigma_ = nullptr;
}

template <typename T>
bool BaseCross2D<T>::set_target_function(TargetFunc2D<T> * const targetFunc) {
    if (nullptr != targetFunc) {
        generator_fm_ = targetFunc;
        return true;
    }
    generator_fm_ = nullptr;
    return false;
}

template <typename T>
bool BaseCross2D<T>::set_guess_rank(const int guessRank) {
    if (guessRank > 1) {
        guess_rank_ = guessRank;
        is_guess_rank_setup_ = true;
        return true;
    }
    return false;
}

template <typename T>
void BaseCross2D<T>::set_truncation_accuracy(const double rtEpsilon) {
    rank_trancation_epsilon_ = rtEpsilon;
}

template <typename T>
int BaseCross2D<T>::nx_rows() const {
    return generator_fm_->nx_rows();
}

template <typename T>
int BaseCross2D<T>::my_cols() const {
    return generator_fm_->my_cols();
}

template <typename T>
bool BaseCross2D<T>::do_decompose() {
    return do_decomposition_method_();
}

template <typename T>
const T * BaseCross2D<T>::adress_U() const {
    return up_Ur_.get();
}

template <typename T>
const T * BaseCross2D<T>::adress_V() const {
    return up_Vr_.get();
}

template <typename T>
T * BaseCross2D<T>::release_U() {
    return up_Ur_.release();
}

template <typename T>
T * BaseCross2D<T>::release_V() {
    return up_Vr_.release();
}

template <typename T>
double * BaseCross2D<T>::release_Sigma() {
    return up_Sigma_.release();
}

template <typename T>
double BaseCross2D<T>::accuracy() const {
    return rank_trancation_epsilon_;
}

template <typename T>
T BaseCross2D<T>::approx_value(const int i, const int j) const {

    const int n_rows = generator_fm_->nx_rows();
    const int m_cols = generator_fm_->my_cols();

    T sum_apprx = zero_init<T>();
    for (int k = 0; k < rank_; ++k) {
        sum_apprx += Ur_[i + n_rows * k] * Sigma_[k] * Vr_[j + m_cols * k];
    }
    return sum_apprx;
}

template <typename T>
T BaseCross2D<T>::U(const int i, const int k) const {
    const int n_rows = generator_fm_->nx_rows();
    return Ur_[i + n_rows * k];
}

template <typename T>
double BaseCross2D<T>::Sigma(const int k) const {
    return Sigma_[k];
}

template <typename T>
T BaseCross2D<T>::V(const int j, const int k) const {
    return Vr_[j + rank_ * k];
}

template <typename T>
int BaseCross2D<T>::rank() const {
    return rank_;
}

template <typename T>
int BaseCross2D<T>::guess_rank() const {
    return guess_rank_;
}

template <typename T>
T* BaseCross2D<T>::matvec(T* x, char option) const
{

// 	options
//
// 	f -- full 			 matvec
// 	l -- lower 		triangle matvec
// 	u -- upper 		triangle matvec
// 	p -- lower persymmetric triangle matvec
// 	q -- upper persymmetric triangle matvec
// allocate memory
    int R = this->rank_;
    int M = this->nx_rows();
    int N = this->my_cols();
    T* result = (T*) malloc( M * sizeof(T) );
    T* U = this->Ur_;
    T* V = this->Vr_;
    T w;
        
    if (option == 'f')
    {
        
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
        {
            printf("%lf ", V[i]);
            result[i] = 0.0;
        }
        printf("\n");
        //full matvec
        T *buff  = (T*) malloc( R * sizeof(T) );
        #pragma omp parallel for
        // compute transpose(V)*x
        for (int i = (int (0)); i < R; i++)
        {

            buff[i] = 0.0;
            T s = 0.0;
            //buff#pragma omp parallel for reduction(+:s)
            for (int j = (int (0)); j < N; j++)
                s += V[N * i + j] * x[j];
            buff[i] = s;
        }
        // compute U * (V*x)
            #pragma omp parallel for
            for (int i = (int (0)); i < M; i++)
            {
                for(int j = (int (0)); j < R; j++)
                    result[i] += U[M * j + i] * buff[j] * Sigma_[j];
            }
       //free memory
       free(buff);
       buff = NULL;

    }
    else if (option == 'p')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
    //lower persymmetric triangle
    //#pragma omp parallel for private(w) 
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
	    {
                if (j < N)
                    w += V[N * i + (N - 1 - j)] * x[N - 1 - j];
                   //workspace[M * omp_get_thread_num() * M + j] += w * U[M * i + j];
                result[j] += w * U[M * i + j] * Sigma_[i];
            }    	
	}
    }
    else if (option == 'l')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
        //lower triangle
        //#pragma omp parallel for
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + j] * x[j];
                //workspace[M * omp_get_thread_num() + j] += w * U[ M * i + j]
                result[j] += w * U[M * i + j] * Sigma_[i];
            }
        }
    }
    else if (option == 'u')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
        // upper triangle	
        //#pragma omp parallel for 
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + (N - 1 - j)] * x[N - 1 - j];
                //workspace[M * omp_get_thread_num() + N - 1 - j] += w * U[ M * i + N - 1 - j]
                result[N - 1 - j] += w * U[M * i + N - 1 - j] * Sigma_[i];
            }
        }
    }
    else if (option == 'q')
    {
        #pragma omp parallel for
        for (int i = 0; i < M; i++)
            result[i] = 0.0;
        //upper persymmetric triagnle
        //#pragma omp parallel for 
        for (int i = 0; i < R; i++)
        {
            w = 0.0;
            for (int j = 0; j < M; j++)
            {
                if (j < N)
                    w += V[N * i + j] * x[j];
                //workspace[M * omp_get_thread_num() + N - 1 - j] += w * U[ M * i + N - 1 -j]
                result[N - j - 1] += w * U[M * i + N - j - 1] * Sigma_[i];
            }
        }
    }
    else
    {
        printf("incorrect matvec option\nreturn null\n");
        free(result);
        result = NULL;
    }
    return result;
}

///---------------------------  BaseCross2D<T>  ---------------------------////

///////////////////////////////////////////////////////////////////////////////

///----------------------------  SVDCross2D<T>  ---------------------------////

template <typename T>
SVDCross2D<T>::SVDCross2D():
    BaseCross2D<T>(),
    Usvd_(nullptr),
    Vsvd_(nullptr),
    Ssvd_(nullptr),
    Asvd_(nullptr),
    up_Usvd_(Usvd_),
    up_Vsvd_(Vsvd_),
    up_Ssvd_(Ssvd_),
    up_Asvd_(Asvd_),
    Work_(nullptr),
    up_Work_(Work_),
    Rwork_(nullptr),
    up_Rwork_(Rwork_),
    alloc_lwork_(0),
    alloc_rows_(0),
    alloc_cols_(0),
    alloc_rank_(0),
    alloc_n_r_(0),
    alloc_r_m_(0),
    alloc_sr_(0) {
}

template <typename T>
SVDCross2D<T>::~SVDCross2D() {}

template <typename T>
void SVDCross2D<T>::release_base_memory() {

    alloc_n_r_  = 0;
    alloc_r_m_  = 0;
    alloc_sr_   = 0;

    BaseCross2D<T>::release_base_memory();
}

template <typename T>
void SVDCross2D<T>::release_auxiliary_memory() {

    alloc_lwork_ = 0;
    alloc_rows_  = 0;
    alloc_cols_  = 0;
    alloc_rank_  = 0;

    // Release memory
    up_Usvd_.reset(nullptr);
    up_Vsvd_.reset(nullptr);
    up_Asvd_.reset(nullptr);
    up_Ssvd_.reset(nullptr);
    up_Work_.reset(nullptr);
    up_Rwork_.reset(nullptr);

    Usvd_  = nullptr;
    Vsvd_  = nullptr;
    Asvd_  = nullptr;
    Ssvd_  = nullptr;
    Work_  = nullptr;
    Rwork_ = nullptr;
}

//virtual
template <typename T>
bool SVDCross2D<T>::set_guess_matrix_U(T * const) {return false;}

//virtual
template <typename T>
bool SVDCross2D<T>::set_guess_matrix_V(T * const) {return false;}

//virtual
template <typename T>
void SVDCross2D<T>::set_iter_limit(const int) {}

template <typename T>
bool SVDCross2D<T>::do_decomposition_method_() {

    if (!BaseCross2D<T>::generator_fm_) {return false;}

    // 1. Reallocate memory if needed
    realloc_svd_arrays_();
    // 2. Fill the matrix from function values
    compute_full_matrix_A_();
    // 3. SVD
    direct_svd_of_A_();
    // 4. Rounding
    truncate_svd_Sigma_();
    // 5. bind data;
    reallocate_base_arrays_();
    bind_interface_matrices_();

    return true;
}

template <typename T>
void SVDCross2D<T>::realloc_svd_arrays_() {

    // Temporary sizes
    const int n_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int m_cols = BaseCross2D<T>::generator_fm_->my_cols();

    const int nn_sz = n_rows * n_rows;
    const int sz_mm = m_cols * m_cols;
    const int n_x_m_sz = n_rows * m_cols;

    int & rrank = BaseCross2D<T>::rank_;
    rrank = std::min<int>(n_rows, m_cols);

    bool row_fl = false;
    bool col_fl = false;
    // Left matrix U

    if (n_rows > alloc_rows_) {
        alloc_rows_ = n_rows;
        raw_reallocate<T>(Usvd_, nn_sz, up_Usvd_);
        row_fl = true;
    }

    // Right matrix Vt
    if (m_cols > alloc_cols_) {
        alloc_cols_ = m_cols;
        raw_reallocate<T>(Vsvd_, sz_mm, up_Vsvd_);
        col_fl = true;
    }
    // Singular values
    if (rrank > alloc_rank_) {
        alloc_rank_ = rrank;
        raw_reallocate<double>(Ssvd_, rrank, up_Ssvd_);
    }
        // Full Matrix A
    if (row_fl || col_fl) {
        raw_reallocate<T>(Asvd_, n_x_m_sz, up_Asvd_);
    }

    // SVD buffers. Factor 20, see lapack documentation.
    int l_work_tmp = 20 * std::max<int>(n_rows, m_cols);
    if (l_work_tmp > alloc_lwork_) {
        alloc_lwork_ = l_work_tmp;
        raw_reallocate<T>(Work_, alloc_lwork_, up_Work_);
        raw_reallocate<double>(Rwork_, alloc_lwork_, up_Rwork_);
    }
}

template <typename T>
void SVDCross2D<T>::reallocate_base_arrays_() {

    const int n_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int m_cols = BaseCross2D<T>::generator_fm_->my_cols();
    int & rrank = BaseCross2D<T>::rank_;

    const int n_r = n_rows * rrank;
    if (n_r > alloc_n_r_) {
        alloc_n_r_ = n_r;
        raw_reallocate<T>(BaseCross2D<T>::Ur_, n_r, BaseCross2D<T>::up_Ur_);
    }

    const int r_m = rrank * m_cols;
    if (r_m > alloc_r_m_) {
        alloc_r_m_ = r_m;
        raw_reallocate<T>(BaseCross2D<T>::Vr_, r_m, BaseCross2D<T>::up_Vr_);
    }

    if (rrank > alloc_sr_) {
        alloc_sr_ = rrank;
        raw_reallocate<double>(BaseCross2D<T>::Sigma_, rrank, BaseCross2D<T>::up_Sigma_);
    }
}

template <typename T>
void SVDCross2D<T>::compute_full_matrix_A_() {
    const int nrows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int mcols = BaseCross2D<T>::generator_fm_->my_cols();
    for (int i = 0; i < nrows; ++i) {
        for (int j = 0; j < mcols; ++j) {
            Asvd_[i + nrows * j] = BaseCross2D<T>::generator_fm_->value(i,j);
        }
    }
}

template <typename T>
bool SVDCross2D<T>::direct_svd_of_A_() {return false;}

template <>
bool SVDCross2D<double>::direct_svd_of_A_() {

    char U_mode  = 'S';
    char Vt_mode = 'S';
    int nrows = BaseCross2D<double>::generator_fm_->nx_rows();
    int mcols = BaseCross2D<double>::generator_fm_->my_cols();
    int svd_rank = std::min<int>(nrows, mcols);
    int info = 0;

    dgesvd_(&U_mode, &Vt_mode, &nrows, &mcols, Asvd_, &nrows, Ssvd_,
            Usvd_, &nrows, Vsvd_, &svd_rank, Work_, &alloc_lwork_, &info);

    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << "SVD<double> did not converged..." << endl;
        }
        return false;
    }
    return true;
}

template <>
bool SVDCross2D<dcomplex>::direct_svd_of_A_() {

    char U_mode  = 'S';
    char Vt_mode = 'S';
    int nrows = BaseCross2D<dcomplex>::generator_fm_->nx_rows();
    int mcols = BaseCross2D<dcomplex>::generator_fm_->my_cols();
    int svd_rank = std::min<int>(nrows, mcols);
    int info = 0;

    zgesvd_(&U_mode, &Vt_mode, &nrows, &mcols, Asvd_, &nrows, Ssvd_,
            Usvd_, &nrows, Vsvd_, &svd_rank, Work_, &alloc_lwork_, Rwork_, &info);

    if (0 != info) {
        if (Verbose::UserInform::show()) {cout << "SVD<dcomplex> did not converged" << endl;}
        return false;
    }
    return true;
}

template <typename T>
void SVDCross2D<T>::truncate_svd_Sigma_() {

    int & rrank = BaseCross2D<T>::rank_;
    const double svd_norm = sqrt_sum2(Ssvd_, rrank);

    // Trancation
    const double relative_condition = BaseCross2D<T>::rank_trancation_epsilon_ * svd_norm;
    for (int kr = 0; kr < rrank; ++kr) {
        if (Ssvd_[kr] < relative_condition) {
            rrank = kr;
            return;
        }
    }
}

template <typename T>
void SVDCross2D<T>::bind_interface_matrices_() {

    const int n_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int m_cols = BaseCross2D<T>::generator_fm_->my_cols();
    const int rankc  = BaseCross2D<T>::rank_;
    const int n_r = n_rows * rankc;

    pure_copy<T>(Usvd_, BaseCross2D<T>::Ur_, n_r);
    pure_copy<double>(Ssvd_, BaseCross2D<T>::Sigma_, rankc);
    // Vrh is Hermitian conj
    for (int ic = 0; ic < m_cols; ++ic) {
        pure_copy<T>(&Vsvd_[ic * m_cols], &BaseCross2D<T>::Vr_[ic * rankc], rankc);
    }
}

///////////////////////////////////////////////////////////////////////////////

template <typename T> StableCross2D<T>::StableCross2D():
    BaseCross2D<T>(),
    is_Ur_setup_(false),
    is_Vr_setup_(false),
    base_alloc_rank_(0),
    auxiliary_alloc_rank_(0),
    lwork_size_x_(0),
    iterLimit_(1024),
    Ur_copy_(nullptr),
    Vr_copy_(nullptr),
    up_Ur_copy_(Ur_copy_),
    up_Vr_copy_(Vr_copy_),
    I_u_(nullptr),
    I_v_(nullptr),
    up_I_u_(I_u_),
    up_I_v_(I_v_),
    buffer_(nullptr),
    up_buffer_(buffer_),
    sd_buff_(nullptr),
    up_sd_buff_(sd_buff_),
    A_hat_(nullptr),
    Qu_hat_(nullptr),
    Qv_hat_(nullptr),
    up_A_hat_(A_hat_),
    up_Qu_hat_(Qu_hat_),
    up_Qv_hat_(Qv_hat_),
    old_singular_sgm_(nullptr),
    up_old_singular_sgm_(old_singular_sgm_),
    old_M_rank_(0),
    M_rank_(0) {
}

//virtual
template <typename T> StableCross2D<T>::~ StableCross2D() {
}

//virtual
template <typename T>
void StableCross2D<T>::release_base_memory() {
    base_alloc_rank_ = 0;
    BaseCross2D<T>::release_base_memory();
}

//virtual
template <typename T>
void StableCross2D<T>::release_auxiliary_memory() {

    auxiliary_alloc_rank_ = 0;

    up_Ur_copy_.reset(Ur_copy_ = nullptr);
    up_Vr_copy_.reset(Vr_copy_ = nullptr);
    up_I_u_.reset(I_u_ = nullptr);
    up_I_v_.reset(I_v_ = nullptr);
    up_buffer_.reset(buffer_ = nullptr);
    up_sd_buff_.reset(sd_buff_ = nullptr);
    up_A_hat_.reset(A_hat_ = nullptr);
    up_Qu_hat_.reset(Qu_hat_ = nullptr);
    up_Qv_hat_.reset(Qv_hat_ = nullptr);
    up_old_singular_sgm_.reset(old_singular_sgm_ = nullptr);
}

//virtual
template <typename T>
bool StableCross2D<T>::set_guess_matrix_U(T * const U_init) {

    BaseCross2D<T>::rank_ = 0; // Must be done before the reallocation.
    if (!preliminarily_init_alloc_()) {return false;}
    BaseCross2D<T>::rank_ = BaseCross2D<T>::guess_rank_;
    // Will be reset for empty V in random filling in the same value.

    // Copy the input matrix.
    T * & Ur  = BaseCross2D<T>::Ur_;
    const int nx_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int crank   = BaseCross2D<T>::guess_rank_;

    pure_copy<T>(U_init, Ur, nx_rows * crank);

    is_Ur_setup_ = true;
    return true;
}

//virtual
template <typename T>
bool StableCross2D<T>::set_guess_matrix_V(T * const V_init) {

    BaseCross2D<T>::rank_ = 0; // Must be done before the reallocation.
    if (!preliminarily_init_alloc_()) {return false;}
    BaseCross2D<T>::rank_ = BaseCross2D<T>::guess_rank_;
    // Will be reset for empty V in random filling in the same value.

    // Copy the input matrix.
    T * & Vr  = BaseCross2D<T>::Vr_;
    const int my_cols = BaseCross2D<T>::generator_fm_->my_cols();
    const int crank   = BaseCross2D<T>::guess_rank_;

    pure_copy<T>(V_init, Vr, my_cols * crank);

    is_Vr_setup_ = true;
    return true;
}

//virtual
template <typename T>
void StableCross2D<T>::set_iter_limit(const int extItrLmt) {
    if (extItrLmt > 0) {
        iterLimit_ = extItrLmt;
    }
}

template <typename T>
bool StableCross2D<T>::preliminarily_init_alloc_() {

    // 1. Check the target function setup.
    if (nullptr == BaseCross2D<T>::generator_fm_) {
        if (Verbose::UserInform::show()) {
            cout << " StableCross2D Error!: TargetFunction has not been set." << endl;
        }
        return false;
    }

    // 2. Check if the guess_rank setup.
    if (!BaseCross2D<T>::is_guess_rank_setup_) {
        if (Verbose::UserInform::show()) {
            cout << " StableCross2D Error! set U or V: The guess rank has not been set." << endl;
        }
        return false;
    }

    // 3. Allocate memory (if needed).
    if (!allocate_extra_memory_(BaseCross2D<T>::guess_rank_)) {
        if (Verbose::UserInform::show()) {
            cout << "Matrix U or V input setup memory fail.." << endl;
        }
        return false;
    }
    return true;
}

// The memory can be allocated from the previous decomposition.
// It is not deallocated by default here. If you need to do that
// (you know, for example, that the new decomposition **apriory**
// takes less memory, and it is reasonable) use the release_data()
// routine exactly before the do_decompose() call outside of
// the cross method, in your particular solver.
template <typename T>
bool StableCross2D<T>::do_decomposition_method_() {

    // 0.0 Auxiliary data.
    T * & Ur = BaseCross2D<T>::Ur_;
    T * & Vr = BaseCross2D<T>::Vr_;
    int & rrank = BaseCross2D<T>::rank_;
    rrank = 0;
    const int nx_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int my_cols = BaseCross2D<T>::generator_fm_->my_cols();
    const int min_sz_mn = std::min(nx_rows, my_cols);

    // 0.1 Check the target function setup.
    if (nullptr == BaseCross2D<T>::generator_fm_) {
        if (Verbose::UserInform::show()) {
            cout << " StableCross2D Error!: TargetFunction has not been set." << endl;
        }
        return false;
    }
    // 0.2 Check if the guess_rank setup.
    if (!BaseCross2D<T>::is_guess_rank_setup_) {
        if (Verbose::UserInform::show()) {
            cout << " StableCross2D Error! The guess rank has not been set." << endl;
        }
        return false;
    }

    // 0.3 Initial setup (from input or random).
    if (!setup_from_input_otherwise_random_()) {return false;}
    // The rank = guess_rank is set here

    // Main loop
    int K_rank = 0;      // rank of the SVD M_hat for a given accuracy
    int K_old_rank = 0;  // old rank to estimate convergence
    int k_iter;
    for (k_iter = 0; k_iter < iterLimit_; ++k_iter) {

        // 0.99(9) Check A-matrix oversizing
        if ((2 * rrank) > min_sz_mn) {
            drop_setup_flags_();
            if (!full_rank_matrix_svd_()) {return false;}
            return true;
        }
        if (Verbose::TechnicalDetails::show()) {cout << " k_iter = " << k_iter << endl << endl << endl;}

        // 1.  Normal extention U and V^T*;
        // 1.1 Find indexes of "good" columns/rows in U/V.
        pure_maxvol(Ur_copy_, nx_rows, rrank, I_u_);   // rrank -> rank_inc
        pure_maxvol(Vr_copy_, my_cols, rrank, I_v_);
        // 1.2 Extend the memory:  rank += increment
        if (!allocate_extra_memory_(2 * rrank)) {return false;}
        // 1.3 Add new "good" columns/rows found in 1.1
        //     into U/V using maxvol (corresponding) V/U-indexes
        maxvol_complement_Ur_x2_(Ur, nx_rows, I_v_);
        maxvol_complement_Vr_x2_(Vr, my_cols, I_u_);
        // 1.4 Reset rank to the new (increased) one (added cols/rows are included).
        rrank *= 2;

        // 2. Orthogonalization of U and V marices
        if (!qr_decomposition_Q<T>(Ur, nx_rows, rrank, Ur_copy_, buffer_, lwork_size_x_)) {return false;}
        if (!qr_decomposition_Q<T>(Vr, my_cols, rrank, Vr_copy_, buffer_, lwork_size_x_)) {return false;}

        // 3. Find Approximant M_(r+r_inc)_x_(r+r_inc)
        //    from the equation A_hat = Qu_hat * M_hat * Qv_hat^h.
        // 3.1 maxvol in "extended" matrices
        pure_copy<T>(Ur, Ur_copy_, nx_rows * rrank);
        pure_maxvol(Ur_copy_, nx_rows, rrank, I_u_);
        pure_copy<T>(Vr, Vr_copy_, my_cols * rrank);
        pure_maxvol(Vr_copy_, my_cols, rrank, I_v_);
        // 3.2 Create X_hats
        create_A_hat_();
        extract_Q_hat_(Qu_hat_, Ur, nx_rows, I_u_);
        extract_Q_hat_(Qv_hat_, Vr, my_cols, I_v_);
        // 3.3 Find M_hat in two steps
        if (!solve_M_hat_()) {return false;}

        // 4.  Rank trancation
        // 4.0 Copy old singulars (K_rank values);
        pure_copy<double>(BaseCross2D<T>::Sigma_, old_singular_sgm_, K_rank);

        // 4.1 SVD of M_hat to get the rank of the M_hat approximant
        if (!svd_M_approximant_()) {return false;}
        // 4.2 Find rank via singular values truncation.
        K_old_rank = K_rank;
        K_rank = find_rank_from_sigma(BaseCross2D<T>::Sigma_, rrank,
                                      BaseCross2D<T>::rank_trancation_epsilon_);
        if (Verbose::TechnicalDetails::show()) {
            cout << "StableCross2D: K_rank = " << K_rank << endl;
            print_singulars_(K_rank, K_old_rank);
        }

        // 4.3 Proper basis extension (by proper rank)
        Qx_mult_svd_factor_Mhatx<T>('N', nx_rows, K_rank, rrank, Ur, nx_rows, Qu_hat_, rrank , Ur_copy_, nx_rows);
        pure_copy<T>(Ur_copy_, Ur, nx_rows * K_rank);
        Qx_mult_svd_factor_Mhatx<T>('T', my_cols, K_rank, rrank, Vr, my_cols, Qv_hat_, rrank , Vr_copy_, my_cols);
        pure_copy<T>(Vr_copy_, Vr, my_cols * K_rank);
        // 4.4 Rank update after truncation
        rrank = K_rank;

        // 5.   Loop break condition
        // 5.1  Norm of singular-values vector difference (old - current)
        const double sgm_error_1 = F_norm_sigma_error_(K_old_rank, K_rank);
        const double sgm_norm_1  = sqrt_sum2(BaseCross2D<T>::Sigma_, K_rank);
        const double convergence_criteria = sgm_norm_1 * BaseCross2D<T>::rank_trancation_epsilon_;

        if (Verbose::TechnicalDetails::show()) {
            cout << " Norm of the approximant       = " << sgm_norm_1  << endl;
            cout << " Exit Condition (norm * eps)   = " << convergence_criteria << endl;
            cout << " Frobenius norm singular error = " << sgm_error_1 << endl;
        }
        // 5.2  Check if it is ready to stop.
        if (sgm_error_1 < convergence_criteria) {
            // The algorithm is converged!
            drop_setup_flags_();
            return true;
        }
    }

    if (Verbose::UserInform::show()) {
        cout << " StableCross2D - Error! Maximum iterations limit exceeded." << endl;
        cout << " StableCross2D did not converge." << endl;
        cout << " Iterations number = " << k_iter << endl;
    }
    return false;
}

template <typename T>
void StableCross2D<T>::drop_setup_flags_() {
    is_Ur_setup_ = false;
    is_Vr_setup_ = false;
    BaseCross2D<T>::is_guess_rank_setup_ = false;
}

template <typename T>
bool StableCross2D<T>::setup_from_input_otherwise_random_() {

    // 0. Prepare data
    T * & Urr = BaseCross2D<T>::Ur_;
    T * & Vrr = BaseCross2D<T>::Vr_;
    int & rrank = BaseCross2D<T>::rank_;
    const int nx_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int my_cols = BaseCross2D<T>::generator_fm_->my_cols();

    // 0.1 Memory reallocation
    // Allocate memory in advance: columns x 2 (next step in main loop)
    const int g_rnk = BaseCross2D<T>::guess_rank();
    if (!allocate_extra_memory_(g_rnk)) {return false;};
    rrank = g_rnk;

    // Ur random setup if needed
    if (!is_Ur_setup_) {
        if (Verbose::UserInform::show()) {
            cout << "Setup U matrix from random with guess rank = " << rrank << endl;
        }
        random_fill_in<T>(Urr, nx_rows, rrank);
    }

    // Vr random setup if needed
    if (!is_Vr_setup_) {
        if (Verbose::UserInform::show()) {
            cout << "Setup V matrix from random with guess rank = " << rrank << endl;
        }
        random_fill_in<T>(Vrr, my_cols, rrank);
    }

    // 0.3 Orthogonalization
    if (!qr_decomposition_Q<T>(Urr, nx_rows, rrank, Ur_copy_, buffer_, lwork_size_x_)) {return false;}
    if (!qr_decomposition_Q<T>(Vrr, my_cols, rrank, Vr_copy_, buffer_, lwork_size_x_)) {return false;}

    // 0.4 Prepare for maxvol which modifies the input matrix.
    pure_copy(Urr, Ur_copy_, nx_rows * rrank);
    pure_copy(Vrr, Vr_copy_, my_cols * rrank);

    return true;
}

template <typename T>
bool StableCross2D<T>::allocate_extra_memory_(const int size_new) {

    const int crank =  BaseCross2D<T>::rank_;

    if (Verbose::TechnicalDetails::show()) {
        cout << " StableCross2D<T>::allocate_extra_memory_:" << endl;
        cout << "  current_rank = "  << crank;
        cout << "  new_size = " << size_new;
        cout << "  base_alloc_rank = "  << base_alloc_rank_;
        cout << "  auxiliary_alloc_rank = "  << auxiliary_alloc_rank_ <<  endl;
    }

    if (!allocate_base_extra_memory_(size_new)) {return false;}
    if (!allocate_auxiliary_extra_memory_(size_new)) {return false;}

    return true;
}

template <typename T>
bool StableCross2D<T>::allocate_base_extra_memory_(const int size_new) {

    const int crank =  BaseCross2D<T>::rank_;

    if (size_new > base_alloc_rank_) {

        // Recompute new memory base rank
        increse_allocated_rank_(base_alloc_rank_, size_new);

        // Reallocate base memory
        if (!reallocate_base_memory_(crank, base_alloc_rank_)) {
            if (Verbose::UserInform::show()) {
                cout << " \tStableCross2D<T>::reallocate_base_memory_ failed with ";
                cout << " \trank_ = " <<  crank;
                cout << " \tand base_alloc_rank_ = " << base_alloc_rank_ << endl;
            }
            return false;
        }
    }
    return true;
}

template <typename T>
bool StableCross2D<T>::allocate_auxiliary_extra_memory_(const int size_new) {

    const int crank =  BaseCross2D<T>::rank_;

    if (size_new > auxiliary_alloc_rank_) {

        // Recompute new memory auxiliary rank
        increse_allocated_rank_(auxiliary_alloc_rank_, size_new);

        // Reallocate auxiliary memory
        if (!reallocate_auxiliary_memory_(crank, auxiliary_alloc_rank_)) {
            if (Verbose::UserInform::show()) {
                cout << " \tStableCross2D<T>::reallocate_auxiliary_memory_ failed with ";
                cout << " \trank_ = " <<  crank;
                cout << " \tand auxiliary_alloc_rank_ = " << auxiliary_alloc_rank_ << endl;
            }
            return false;
        }
    }
    return true;
}

template <typename T>
bool StableCross2D<T>::reallocate_base_memory_(const int old_rank, const int rank_new) {

    // Message
    if (Verbose::TechnicalDetails::show()) {
        cout << " \tStableCross2D<T>::reallocate_base_memory:" << endl;
        cout << " \tfrom old_rank = "<< old_rank;
        cout << " \tto new_size = " << rank_new << endl;
    }

    // Just short constnants
    const int nx = BaseCross2D<T>::generator_fm_->nx_rows();
    const int my = BaseCross2D<T>::generator_fm_->my_cols();

    const int nx_x_or_Ur = nx * old_rank;
    const int nx_x_rn_Ur = nx * rank_new;
    const int my_x_or_Vr = my * old_rank;
    const int my_x_rn_Vr = my * rank_new;

    // Ur, Vr uses buffer_
    if (!hist_reallocate<T>(BaseCross2D<T>::Ur_, nx_x_or_Ur, nx_x_rn_Ur,
                            BaseCross2D<T>::up_Ur_)) {return false;}
    if (!hist_reallocate<T>(BaseCross2D<T>::Vr_, my_x_or_Vr, my_x_rn_Vr,
                            BaseCross2D<T>::up_Vr_)) {return false;}
    // Singular values
    if (!hist_reallocate<double>(BaseCross2D<T>::Sigma_, old_rank, rank_new,
                                 BaseCross2D<T>::up_Sigma_)) {return false;}
    return true; // Successful return;
}

template <typename T>
bool StableCross2D<T>::reallocate_auxiliary_memory_(const int old_rank, const int rank_new) {

    // Message
    if (Verbose::TechnicalDetails::show()) {
        cout << " \tStableCross2D<T>::reallocate_auxiliary_memory:" << endl;
        cout << " \tfrom old_rank = "<< old_rank;
        cout << " \tto new_size = " << rank_new << endl;
    }

    // Just short constnants
    const int nx = BaseCross2D<T>::generator_fm_->nx_rows();
    const int my = BaseCross2D<T>::generator_fm_->my_cols();
    const int nm_max = std::max<int>(nx, my);

    const int nx_x_rn_Ur = nx * rank_new;
    const int my_x_rn_Vr = my * rank_new;

    // Reallocation
    const int uv_max_sz = nm_max * rank_new;
    lwork_size_x_ = uv_max_sz;

    // Buffers for swap/buffering many arrays. Are used in QR, SVD
    if (!raw_reallocate<T>(buffer_, uv_max_sz, up_buffer_)) {return false;}
    if (!raw_reallocate<double>(sd_buff_, 6 * rank_new, up_sd_buff_)) {return false;}

    // Maxvol use Ur/Vr_copy. Size can be changed later.
    if (!raw_reallocate<T>(Ur_copy_, nx_x_rn_Ur, up_Ur_copy_)) {return false;}
    if (!raw_reallocate<T>(Vr_copy_, my_x_rn_Vr, up_Vr_copy_)) {return false;}

    // Maxvol
    if (!hist_reallocate<int>(I_u_, old_rank, rank_new, up_I_u_)) {return false;}
    if (!hist_reallocate<int>(I_v_, old_rank, rank_new, up_I_v_)) {return false;}

    // Hats
    const int max_rank2 = auxiliary_alloc_rank_ * auxiliary_alloc_rank_;
    if (!raw_reallocate<T>(A_hat_,  max_rank2, up_A_hat_) ) {return false;}
    if (!raw_reallocate<T>(Qu_hat_, max_rank2, up_Qu_hat_)) {return false;}
    if (!raw_reallocate<T>(Qv_hat_, max_rank2, up_Qv_hat_)) {return false;}

    // Singular values
    if (!hist_reallocate<double>(StableCross2D<T>::old_singular_sgm_, old_rank, rank_new,
                                 StableCross2D<T>::up_old_singular_sgm_)) {return false;}
    return true; // Successful return;
}

template <typename T>
void StableCross2D<T>::increse_allocated_rank_(int & inp_alloc_rank, const int size_new) {

    inp_alloc_rank *= 2;
    if (inp_alloc_rank < size_new) {
        inp_alloc_rank = size_new + 1;
    }
}

template <typename T>
void StableCross2D<T>::maxvol_complement_Ur_x2_(T * const Uq, const int rows_sz,
                                                        const int * const Ind_v) {
    const int rrank = BaseCross2D<T>::rank_;
    const int shift_nr = rows_sz * rrank;

//    #pragma omp parallel for
    for (int k = 0; k < rrank; ++k) {
        const int k_shift = rows_sz * k;
        for (int i = 0; i < rows_sz; ++i) {                        // -1 from Fortran indexing
            Uq[shift_nr + i + k_shift] = BaseCross2D<T>::generator_fm_->value(i, Ind_v[k] - 1);
        }
    }
}

template <typename T>
void StableCross2D<T>::maxvol_complement_Vr_x2_(T * const Vq, const int cols_sz,
                                                        const int * const Ind_u) {
    const int rrank = BaseCross2D<T>::rank_;
    const int shift_mr = cols_sz * rrank;
    for (int k = 0; k < rrank; ++k) {
        const int k_shift = cols_sz * k;
        for (int j = 0; j < cols_sz; ++j) {                        // -1 from Fortran indexing
            Vq[shift_mr + j + k_shift] = BaseCross2D<T>::generator_fm_->value(Ind_u[k] - 1, j);
        }
    }
}

template <typename T>
void StableCross2D<T>::create_A_hat_() {

    const int rrank = BaseCross2D<T>::rank_;
    for (int i = 0; i < rrank; ++i) {
        for (int j = 0; j < rrank; ++j) {

            // Transposed matrix (is needed for the Qv * X = A^t equation or Qv * X = conj(A^t))
            A_hat_[j + i * rrank] = BaseCross2D<T>::generator_fm_->value(I_u_[i] - 1, I_v_[j] - 1);
        }
    }
}

template <typename T>
void StableCross2D<T>::extract_Q_hat_(T * const Qx_hat, const T * const Qx_full,
                                      const int row_sz, const int * const Ix) {
    const int rrank = BaseCross2D<T>::rank_;
    for (int i = 0; i < rrank; ++i) {
        for (int k = 0; k < rrank; ++k) {
            Qx_hat[i + k * rrank] = Qx_full[(Ix[i] - 1) + k * row_sz];
        }
    }
}

template <>
bool StableCross2D<double>::solve_M_hat_() {

    int r_sz = BaseCross2D<double>::rank_;
    int info;

    // 1.  Qv * X = A^t;   A_hat have been transposed already (but not conjugated!)
    dgesv_(&r_sz, &r_sz, Qv_hat_, &r_sz, I_u_, A_hat_, &r_sz, &info);
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " BaseCross2D<double>::solve_M_hat:  Error in dgesv_ " << endl;
            cout << " info code = " << info << endl;
        }
        return false;
    }

    // 2. Transpose X^t (Preliminary step);  X is in A_hat
    transpose_square_matrix<double>(A_hat_, r_sz);

    // 3. Solve Qu * M = X^t;    I_u_ is just a dummy variable
    dgesv_(&r_sz, &r_sz, Qu_hat_, &r_sz, I_u_, A_hat_, &r_sz, &info);
    // now M_hat is in the A_hat
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " BaseCross2D<double>::solve_M_hat:  Error in dgesv_ (2) " << endl;
            cout << " info code = " << info << endl;
        }
        return false;
    }
    return true;  // successful return
}

template <>
bool StableCross2D<dcomplex>::solve_M_hat_() {

    int r_sz = BaseCross2D<dcomplex>::rank_;
    int info;

    // 1.  Qv * X = A^t;   A_hat have been transposed already (but not conjugated!)
    zgesv_(&r_sz, &r_sz, Qv_hat_, &r_sz, I_u_, A_hat_, &r_sz, &info);
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " BaseCross2D<dcomplex>::solve_M_hat:  Error in zgesv_ " << endl;
            cout << " info code = " << info << endl;
        }
        return false;
    }

    // 2. Transpose X^t (Preliminary step);  X is in A_hat. No conjugation is needed
    transpose_square_matrix<dcomplex>(A_hat_, r_sz);

    // 3. Solve Qu * M = X^t;    I_u_ is just a dummy variable
    zgesv_(&r_sz, &r_sz, Qu_hat_, &r_sz, I_u_, A_hat_, &r_sz, &info);
    // now M_hat is in the A_hat
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " BaseCross2D<dcomplex>::solve_M_hat:  Error in zgesv_ (2) " << endl;
            cout << " info code = " << info << endl;
        }
        return false;
    }
    return true;  // successful return
}

template <>
bool StableCross2D<double>::svd_M_approximant_() {

    char U_mode  = 'A';
    char Vt_mode = 'A';
    int  r_sz = rank_;
    int  info;
    double * Singulars = BaseCross2D<double>::Sigma_;

    dgesvd_(&U_mode, &Vt_mode, &r_sz, &r_sz, A_hat_,
            &r_sz, Singulars, Qu_hat_, &r_sz, Qv_hat_, &r_sz,
            buffer_, &lwork_size_x_, &info);

    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " StableCross2D::svd_M_approximant<double>:  Error in dgesvd_ " << endl;
            cout << " info code = " << info << endl;
        }
        return false;
    }
    return true;
}

template <>
bool StableCross2D<dcomplex>::svd_M_approximant_() {

    char U_mode  = 'A';
    char Vt_mode = 'A';
    int  r_sz = rank_;
    int  info;
    double * Singulars = BaseCross2D<dcomplex>::Sigma_;

    zgesvd_(&U_mode, &Vt_mode, &r_sz, &r_sz, A_hat_,
            &r_sz, Singulars, Qu_hat_, &r_sz, Qv_hat_, &r_sz,
            buffer_, &lwork_size_x_, sd_buff_ , &info);

    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " StableCross2D::svd_M_approximant<dcomplex>:  Error in zgesvd_ (3)" << endl;
            cout << " info code = " << info << endl;
        }
        return false;
    }
    return true;
}

template <typename T>
void StableCross2D<T>::print_singulars_(const int K_rank, const int old_k_rnk) {

    const double * const pSgm  = BaseCross2D<T>::Sigma_;
    const double * const osgm  = old_singular_sgm_;
    const int rrank = BaseCross2D<T>::rank_;

    cout << "OLD Singular values :" << endl;
    for (int i = 0; i < old_k_rnk; ++i) {
        cout << setw(14) << osgm[i];
        if ( !((i + 1) % 5) ) {
            cout << endl;
        }
    } cout << endl;

    cout << "Singular values :" << endl;
    for (int i = 0; i < K_rank; ++i) {
        cout << setw(14) << pSgm[i];
        if ( !((i + 1) % 5) ) {
            cout << endl;
        }
    } cout << endl;

    cout << "Singular values (truncated tail):" << endl;
    for (int i = K_rank; i < rrank; ++i) {
        cout << setw(14) << pSgm[i];
        if ( !((i - K_rank + 1) % 5) ) {
            cout << endl;
        }
    } cout << endl;
}

template <typename T>
double StableCross2D<T>::F_norm_sigma_error_(const int old_rank_K, const int rank_K) const {

    const double * const Sgm = BaseCross2D<T>::Sigma_;

    // 1. Calculate differences (old length)
//    cout << "norm sigma error" << endl;
    double sum_diff = 0;
    for (int k = 0; k < old_rank_K; ++k) {
        const double delta_ss = Sgm[k] - this->old_singular_sgm_[k];

//        cout << Sgm[k] << "  " << old_singular_sgm_[k] << " ";
//        cout << "delta_sgm_" << k << " = " << delta_ss << endl;
        sum_diff += delta_ss * delta_ss;
    }

    // 2. Calculate tail
    double sum_tail = 0;
    for (int k = rank_K - 1; k >= old_rank_K; --k) {
        sum_tail += Sgm[k] * Sgm[k];
    }
    return sqrt(sum_diff + sum_tail);
}

template <typename T>
bool StableCross2D<T>::full_rank_matrix_svd_() {

    const int nx_rows = BaseCross2D<T>::generator_fm_->nx_rows();
    const int my_cols = BaseCross2D<T>::generator_fm_->my_cols();
    int & rrank = BaseCross2D<T>::rank_;
    const int sz_min_nm = min(nx_rows, my_cols);

    if (Verbose::UserInform::show()) {
        cout << " StableCross2D<T>: Full rank svd routine at current rank = " << rrank << endl;
        cout << " and " << nx_rows << " x " << my_cols << "  matrix size. " << endl;
    }

    if (!allocate_base_extra_memory_(sz_min_nm)) {return true;}

    // SVD cross approximation
    unique_ptr<SVDCross2D<T> > up_svd_cross(new SVDCross2D<T>());
    SVDCross2D<T> * const svd_cross = up_svd_cross.get();

    // Setup parameters
    svd_cross->set_target_function(this->generator_fm_);
    svd_cross->set_truncation_accuracy(this->accuracy());

    // SVD decomposition
    if (!svd_cross->do_decompose()) {
        if (Verbose::UserInform::show()) {
            cout << " Error in SVD_Cross2D<T> full rank decomposition 1607." << endl;
        }
        return false;
    }

    // Bind Interface
    rrank = svd_cross->rank();
    BaseCross2D<T>::up_Ur_.reset(svd_cross->release_U());
    BaseCross2D<T>::Ur_ = BaseCross2D<T>::up_Ur_.get();

    BaseCross2D<T>::up_Vr_.reset(svd_cross->release_V());
    BaseCross2D<T>::Vr_ = BaseCross2D<T>::up_Vr_.get();

    BaseCross2D<T>::up_Sigma_.reset(svd_cross->release_Sigma());
    BaseCross2D<T>::Sigma_ = BaseCross2D<T>::up_Sigma_.get();

    return true;
}

///////////////////////////////////////////////////////////////////////////////

// Template instantiations (necessary for separate compilation)
template class BaseCross2D<double>;
template class BaseCross2D<dcomplex>;
template class SVDCross2D<double>;
template class SVDCross2D<dcomplex>;
template class StableCross2D<double>;
template class StableCross2D<dcomplex>;

///////////////////////////////////////////////////////////////////////////////

}  // End of namespace matrix_cross_2d

// End of the file

