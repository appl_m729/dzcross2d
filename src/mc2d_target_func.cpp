/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#include <cmath>
#include <iostream>
#include <iomanip>
#include "mc2d_target_func.h"
#include "mc2d_verbose_support.h"

using std::cout;
using std::endl;
using std::setprecision;
using std::setw;

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

template <typename T>
TargetFunc2D<T>::TargetFunc2D():
nx_rows_(1),
my_cols_(1) {
}

//virtual
template <typename T>
TargetFunc2D<T>::~TargetFunc2D() {
}

template <typename T>
bool TargetFunc2D<T>::set_nx_rows(const int nRows) {
    if (nRows > 1) {
        nx_rows_ = nRows;
        return true;
    }
    return false;
}

template <typename T>
bool TargetFunc2D<T>::set_my_cols(const int mCols) {
    if (mCols > 1) {
        my_cols_ = mCols;
        return true;
    }
    return false;
}

template <typename T>
int TargetFunc2D<T>::nx_rows() const {
    return nx_rows_;
}

template <typename T>
int TargetFunc2D<T>::my_cols() const {
    return my_cols_;
}

//virtual
template <typename T>
T TargetFunc2D<T>::value(const int, const int) const {
    return T();
}

/*! Template specialization for double */
template<>
double TargetFunc2D<double>::value(const int, const int) const {
    // Redefine it if needed.
    return -1.;
}

/*! Template specialization for complex<double> */
template<>
dcomplex TargetFunc2D<dcomplex>::value(const int, const int) const {
    // Redefine it if needed.
    return dcomplex(-1,-1);
}

// virtual
template <typename T>
bool TargetFunc2D<T>::are_bounds_setup_properly() const {
    return true;
}

//virtual
template <typename T>
bool TargetFunc2D<T>::init() {
    return true;
}

// Template instantiations (necessary for separate compilation)
template class TargetFunc2D<double>;
template class TargetFunc2D<dcomplex>;

///////////////////////////////////////////////////////////////////////////////

RectangularMesh2D::RectangularMesh2D():
ax_top_(0),
bx_bottom_(1),
ay_left_(0),
by_right_(1),
one_over_nxm1_(1.0),
one_over_mym1_(1.0),
Dx_ab_(0.0),
Dy_ab_(0.0) {
}

//virtual
RectangularMesh2D::~RectangularMesh2D() {}

void RectangularMesh2D::set_ax_top   (const double ax_t) {ax_top_    = ax_t;}
void RectangularMesh2D::set_bx_bottom(const double bx_b) {bx_bottom_ = bx_b;}
void RectangularMesh2D::set_ay_left  (const double ay_l) {ay_left_   = ay_l;}
void RectangularMesh2D::set_by_right (const double by_r) {by_right_  = by_r;}

double RectangularMesh2D::ax_top()    const {return ax_top_;   }
double RectangularMesh2D::bx_bottom() const {return bx_bottom_;}
double RectangularMesh2D::ay_left()   const {return ay_left_;  }
double RectangularMesh2D::by_right()  const {return by_right_; }

///////////////////////////////////////////////////////////////////////////////

template <typename T>
RectTargetFunc2D<T>::RectTargetFunc2D():
TargetFunc2D<T>(),
RectangularMesh2D() {
}

// virtual
template <typename T>
RectTargetFunc2D<T>::~RectTargetFunc2D() {
}

//virtual
template <typename T>
bool RectTargetFunc2D<T>::are_bounds_setup_properly() const {

    if (ax_top()  >= bx_bottom()) {
        if (Verbose::UserInform::show()) {
            cout << "ax = " << ax_top() << "  >=   bx = " << bx_bottom() << endl;
        }
        return false;
    }
    if (ay_left() >= by_right())  {
        if (Verbose::UserInform::show()) {
            cout << "ay = " << ay_left() << "  >=   by = " << by_right() << endl;
        }
        return false;
    }

    if (TargetFunc2D<T>::nx_rows() <= 1) {
        if (Verbose::UserInform::show()) {
            cout << "Too few nx_rows = " << TargetFunc2D<T>::nx_rows() << endl;
        }
        return false;
    }
    if (TargetFunc2D<T>::my_cols() <= 1) {
        if (Verbose::UserInform::show()) {
            cout << "Too few my_cols = " << TargetFunc2D<T>::my_cols() << endl;
        }
        return false;
    }
    return true;
}

//virtual
template <typename T>
bool RectTargetFunc2D<T>::init() {

    one_over_nxm1_ = 1.0;
    one_over_mym1_ = 1.0;
    Dx_ab_ = 0.0;
    Dy_ab_ = 0.0;

    if (are_bounds_setup_properly()) {

        const double rowsd = static_cast<double>(TargetFunc2D<T>::nx_rows() - 1);
        const double colsd = static_cast<double>(TargetFunc2D<T>::my_cols() - 1);
        one_over_nxm1_ = static_cast<double>(1.0) / rowsd;
        one_over_mym1_ = static_cast<double>(1.0) / colsd;
        Dx_ab_ = bx_bottom() - ax_top();
        Dy_ab_ = by_right()  - ay_left();

        return true;
    }
    return false;
}

//virtual
template <typename T>
T RectTargetFunc2D<T>::value(const int , const int ) const {
    return T();
}

template <typename T>
double RectTargetFunc2D<T>::hx() const {
    const double rd = static_cast<double>(TargetFunc2D<T>::nx_rows()) - 1.0;
    return (bx_bottom() - ax_top()) / rd;
}

template <typename T>
double RectTargetFunc2D<T>::hy() const {
    const double cd = static_cast<double>(TargetFunc2D<T>::my_cols()) - 1.0;
    return (by_right() - ay_left()) / cd;
}

template<>
double RectTargetFunc2D<double>::value(const int i, const int j) const {

//    return  i * my_cols() + j;
//    const dcomplex xiy(0.01 * xi, 0.005 * yj);
//    const dcomplex z2(xiy * xiy);
//    return real(exp(z2));
    const double xi = ax_top()  + Dx_ab_ * one_over_nxm1_ * static_cast<double>(i);
    const double yj = ay_left() + Dy_ab_ * one_over_mym1_ * static_cast<double>(j);

    return 1.0;
//    domain  [-2, 4] x [-5, 10]
//    return 1.0;
//    return (i + 1) * (j + 1);
//    return (xi + 1) * (yj + 1);
//    return xi * xi + xi * yj + 4*yj * yj;
//    return exp(-xi * xi - yj * yj);
      return exp(xi * yj);
//    return exp(-sqrt(xi * xi + yj * yj));   // r = 32 for 30 000 x 30 000
//    return 34.0 * xi * xi + 0.117 * yj * yj;
//    return 1.0/(static_cast<double>(i) + j + 1.0);  // r = 27 for 30 000 x 30 000
//    return 1.0/(xi + yj + 1.0);
//    return 1.0 / sqrt(xi * xi + yj * yj + 1.0);   // r = 12 for 30 000 x 30 000
//    return (4.0 + xi * xi * xi)/(2.0 + sin(15.0 * xi * xi + 9.0 * yj * yj * yj)); // r = 12 for 30 000 x 30 000
//    const double nqc = exp(cos(2 - xi + yj * yj));
//    return (4.0 + xi * xi * xi + nqc)/(2.0 + sin(15.0 * xi * xi + 9.0 * yj * yj * yj)); // r = 364 for 3000 x 3000
//    if (i == j) return 1;
//    return 0;
//    if (i == 7 && j == 9) return 1;
//    if (i == 8 && j == 8) return 1;
//    if (i == 9 && j == 7) return 1;
//    return 0;
//    if (i > 2) {return j + 1;}
//    return 0;
//    if (j > 1 || i > 1) {return 0;}

//     Redefine it if needed.
//     return 2.0 * xi * xi + sin(xi - yj * yj) * sin(xi - yj * yj) / (1 + exp(-xi*yj*(yj +1.0)));
     // r = 48 for 10 000 x 10 000
}

template<>
dcomplex RectTargetFunc2D<dcomplex>::value(const int i, const int j) const {

    const double xi = ax_top_  + Dx_ab_ * one_over_nxm1_ * static_cast<double>(i);
    const double yj = ay_left_ + Dy_ab_ * one_over_mym1_ * static_cast<double>(j);

    const dcomplex zI(0.0, 1.0);
    const double ko = 1e+8;

    const double r = sqrt(xi * xi + yj * yj);

//    cout  << ( exp(-zI * ko * r) / r) << endl;

    return exp(-zI * ko * r) * (1.0 + zI * ko * r) / (r*r*r);


//    const dcomplex z(0.01 * xi, 0.005 * yj);
//    const dcomplex z2(z * z);

////    return exp(-z2);

//    const dcomplex w1(xi + yj + 1.0, xi*xi + yj * (xi +yj * yj) );
//    return 1.0 / sqrt(z2 + w1 + 1.0);  // r = 97 for 10 000 x 1000
}

// Template instantiations (necessary for separate compilation)
template class RectTargetFunc2D<double>;
template class RectTargetFunc2D<dcomplex>;

///////////////////////////////////////////////////////////////////////////////

}  // namespace matrix_cross_2d

// End of the file

