/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#include <cstdlib>
#include <iostream>
#include "mc2d_tmatrix_utils.h"
#include "mc2d_verbose_support.h"
#include "mc2d_utils_dlapack.h"
#include "mc2d_utils_zlapack.h"

using std::cout;
using std::endl;

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

double conj(const double x) {return x;}

///////////////////////////////////////////////////////////////////////////////

template <> int      zero_init<int>()      {return 0;}
template <> double   zero_init<double>()   {return 0.0;}
template <> dcomplex zero_init<dcomplex>() {return dcomplex(0.0, 0.0);}

///////////////////////////////////////////////////////////////////////////////

template <>
void random_fill_in<double>(double * const A,
                            const int rows_sz, const int rank) {
    // The whole size
    const int size_nr = rows_sz * rank;
    // Filling up
    for (int i = 0; i < size_nr; ++i) {
        A[i] = rand() % (2 * MAX_MATRIX_VALUE + 1) - MAX_MATRIX_VALUE;
    }
}

template <>
void random_fill_in<dcomplex>(dcomplex * const A,
                              const int rows_sz, const int rank) {
    // The whole size
    const int size_nr = rows_sz * rank;
    // Filling up
    for (int i = 0; i < size_nr; ++i) {
        dcomplex & A_i = A[i];
        A_i.real(rand() % (2 * MAX_MATRIX_VALUE + 1) - MAX_MATRIX_VALUE);
        A_i.imag(rand() % (2 * MAX_MATRIX_VALUE + 1) - MAX_MATRIX_VALUE);
    }
}

/////////////////////////////////////////////////////////////////////////////////

template <>
void pure_copy<double>(double * const Asrc, double * const Adst,
                       const int inp_sz) {
    int xInc = 1;
    int sz = inp_sz;
    dcopy_(&sz, Asrc, &xInc, Adst, &xInc);
}

template <>
void pure_copy<dcomplex>(dcomplex * const Asrc, dcomplex * const Adst,
                         const int inp_sz) {
    int xInc = 1;
    int sz = inp_sz;
    zcopy_(&sz, Asrc, &xInc, Adst, &xInc);
}

/////////////////////////////////////////////////////////////////////////////////

template <>
void pure_maxvol<double>(double * A_modify, const int rows_size,
                         const int cols_rank, int * const ind_out) {
    int    nswap = 1024;      // iterations limit
    double tol   = 1e-2;      // maxvol exit accuracy
    int    rows_sz  = rows_size;
    int    cols_rnk = cols_rank;

    dmaxvol_(A_modify, &rows_sz, &cols_rnk, ind_out, &nswap, &tol);
}

template <>
void pure_maxvol<dcomplex>(dcomplex * A_modify, const int rows_size,
                           const int cols_rank, int * const ind_out) {
    int    nswap = 1024;      // iterations limit
    double tol   = 1e-2;      // maxvol exit accuracy
    int    rows_sz  = rows_size;
    int    cols_rnk = cols_rank;

    zmaxvol_(A_modify, &rows_sz, &cols_rnk, ind_out, &nswap, &tol);
}

/////////////////////////////////////////////////////////////////////////////////

template <typename T>
bool raw_reallocate(T * & Arr, const int alloc_sz, unique_ptr<T []> & X_ptr) {

    try {
        // Release first
        X_ptr.reset(Arr = nullptr);
        // Then reset
        X_ptr.reset(new T[alloc_sz]);
        Arr = X_ptr.get();

    } catch (std::bad_alloc& ba) {
        if (Verbose::UserInform::show()) {
            cout << "raw_reallocate:  bad_alloc caught... " << ba.what() << endl;
        }
        return false;
    }
    return true;  // Successful return
}

// Template instantiations (necessary for separate compilation)
template bool raw_reallocate<double>(double * & Arr, const int alloc_sz,
                                     unique_ptr<double []> & X_ptr);

template bool raw_reallocate<dcomplex>(dcomplex * & Arr, const int alloc_sz,
                                       unique_ptr<dcomplex []> & X_ptr);

/////////////////////////////////////////////////////////////////////////////////

template <>
bool hist_reallocate<int>(int * & Arr, const int old_sz, const int alloc_sz,
                          unique_ptr<int []> & X_ptr) {
    // Save old memory
    unique_ptr<int []> Buff_ptr(X_ptr.release());

    if (!raw_reallocate<int>(Arr, alloc_sz, X_ptr)) {return false;}

    // Copy old elements
    int * old_Ind = Buff_ptr.get();
    for (int k = 0; k < old_sz; ++k) {
        Arr[k] = old_Ind[k];
    }
    old_Ind = nullptr;

    // The old memory is destroyed automatically with Buff_ptr.
    return true;  // Successful return
}

template <>
bool hist_reallocate<double>(double * & Arr, const int old_sz, const int alloc_sz,
                             unique_ptr<double []> & X_ptr) {
    // Save old memory
    unique_ptr<double []> Buff_ptr(X_ptr.release());

    // Reallocate memory
    if (!raw_reallocate<double>(Arr, alloc_sz, X_ptr)) {return false;}

    // Copy old elements to the new memory
    int xInc = 1;
    int old_size = old_sz;
    double * buffer = Buff_ptr.get();
    dcopy_(&old_size, buffer, &xInc, Arr, &xInc);
    buffer = nullptr;

    // The old memory is destroyed automatically with Buff_ptr.
    return true;
}

template <>
bool hist_reallocate<dcomplex>(dcomplex * & Arr, const int old_sz, const int alloc_sz,
                                unique_ptr<dcomplex []> & X_ptr) {
    // Save old memory
    unique_ptr<dcomplex []> Buff_ptr(X_ptr.release());

    // Reallocate memory
    if (!raw_reallocate<dcomplex>(Arr, alloc_sz, X_ptr)) {return false;}

    // Copy old elements to the new memory
    int xInc = 1;
    int old_size = old_sz;
    dcomplex * buffer = Buff_ptr.get();
    zcopy_(&old_size, buffer, &xInc, Arr, &xInc);
    buffer = nullptr;

    // The old memory is destroyed automatically with Buff_ptr.
    return true;
}

/////////////////////////////////////////////////////////////////////////////////

template <>
bool qr_decomposition_Q<double>(double * const A, const int nrows, const int kcols,
                                double * const buff_1, double * const buff_2,
                                const int lwork_2) {
    // Auxiliary variables
    int  info;
    int  nr = nrows;
    int  mc = kcols;
    int  lw_sz = lwork_2;

    // QR decomposition
    dgeqrf_(&nr, &mc, A, &nr, buff_1, buff_2, &lw_sz, &info);
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << "Error in dgeqrf_(...)  info = " << info << endl;
        }
        return false;
    }

    // Q is in matrix A in a "rotate format" (see lapack documentation)
    int k_refl = mc;            // buff_1, buff_2 must be exactly in the same
                                // order from the dgeqrf.
    dorgqr_(&nr, &mc, &k_refl, A, &nr, buff_1, buff_2, &lw_sz, &info);
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " Error in dorgqr_(...) info = " << info << endl;
            cout << " nrows = " << nr << "  mcols = " << mc << endl;
        }
        return false;
    }
    return true;
}

template <>
bool qr_decomposition_Q<dcomplex>(dcomplex * const A, const int nrows,
                                  const int kcols, dcomplex * const buff_1,
                                  dcomplex * const buff_2, const int lwork_2) {
    // Auxiliary variables
    int  info;
    int  nr = nrows;
    int  mc = kcols;
    int  lw_sz = lwork_2;

    // QR decomposition
    zgeqrf_(&nr, &mc, A, &nr, buff_1, buff_2, &lw_sz, &info);
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << "Error in zgeqrf_(...)  info = " << info << endl;
        }
        return false;
    }

    // Q is in matrix A in a "rotate format" (see lapack documentation)
    int k_refl = mc;            // buff_1, buff_2 must be exactly in the same
                                // order from the dgeqrf.
    zungqr_(&nr, &mc, &k_refl, A, &nr, buff_1, buff_2, &lw_sz, &info);
    if (0 != info) {
        if (Verbose::UserInform::show()) {
            cout << " Error in zungqr_(...) info = " << info << endl;
            cout << " nrows = " << nr << "  mcols = " << mc << endl;
        }
        return false;
    }
    return true;
}

/////////////////////////////////////////////////////////////////////////////////

template <typename T>
void  transpose_square_matrix(T * const A_io, const int n_sz) {

    for (int i = 0; i < n_sz; ++i) {
        for (int j = i; j < n_sz; ++j) {
            const T a_ij = A_io[i + n_sz * j];
            const T a_ji = A_io[j + n_sz * i];
            A_io[i + n_sz * j] = a_ji;
            A_io[j + n_sz * i] = a_ij;
        }
    }
}

// Template instantiations (necessary for separate compilation)
template void transpose_square_matrix<double>(double * const A, const int n_sz);

template void transpose_square_matrix<dcomplex>(dcomplex * const A, const int n_sz);

/////////////////////////////////////////////////////////////////////////////////

/*! Return sqrt(a1 * a1 + ... + an * an) */
double sqrt_sum2(double * const Ss, const int rank_K) {

    int inc = 1;
    int size = rank_K;
    double * arr = Ss;

    return dnrm2_(&size, arr, &inc);
}

/////////////////////////////////////////////////////////////////////////////////

/*! Estimate rank in Frobenious norm by singular values and given epsilon. */
int find_rank_from_sigma(double * const Sigma, const int rank, const double eps) {

    // 0. Check zero-norm matrix.
    const double sgm_0 = Sigma[0];

    // 1. Compute full Frobenius norm of Sigma matrix.
    const double sq_full_sm = sqrt_sum2(Sigma, rank);

    int Kr = 0;

    // Direct x^2 summation
    if (sgm_0 < 1e-20) {

        // 2. Search rank by singular values truncation.
        double partial_sum = 0;
        for (Kr = rank - 1; Kr >= 0; --Kr) {

            const double sgm_k = Sigma[Kr];
            partial_sum += sgm_k * sgm_k;
            const double sqrt_partial_sum = sqrt(partial_sum);

            if (sqrt_partial_sum > sq_full_sm * eps) {break;}
        }

    // smart summation
    } else {
        // 2. Search rank by singular values truncation.
        double partial_sum = 0;
        for (Kr = rank - 1; Kr >= 0; --Kr) {

            const double sigm_i_0 = Sigma[Kr] / sgm_0;
            partial_sum += sigm_i_0 * sigm_i_0;
            const double sqrt_partial_sum = sgm_0 * sqrt(partial_sum);

            if (sqrt_partial_sum > sq_full_sm * eps) {break;}
        }
    }
    return Kr + 1;
}

/////////////////////////////////////////////////////////////////////////////////

template <>
void Qx_mult_svd_factor_Mhatx<double>(const char op_M_factor,
                                      const int n_rows_A, const int m_cols_B, const int k_cA_rB,
                                      double * A, const int ldA, double * B, const int ldB,
                                      double * C, const int ldC) {
    char trans_A = 'N';
    char trans_B =  op_M_factor;   // 'T' - is transposed, 'N' - no modification, 'H' - hermite tr
    double alpha = 1.0;            // C = alpha * A * B^T + beta * C
    double beta  = 0.0;
    int Q_rows     = n_rows_A;     // rows of matrix Q
    int MhatX_cols = m_cols_B;     // columns of matrix C
    int Qc_MXr     = k_cA_rB;      // rows of matrix B == columns of matrix A
    int lda = ldA;
    int ldb = ldB;
    int ldc = ldC;

    dgemm_(&trans_A, &trans_B, &Q_rows, &MhatX_cols, &Qc_MXr,
                   &alpha, A, &lda, B, &ldb, &beta, C, &ldc);
}

template <>
void Qx_mult_svd_factor_Mhatx<dcomplex>(const char op_M_factor,
                                        const int n_rows_A, const int m_cols_B, const int k_cA_rB,
                                        dcomplex * A, const int ldA, dcomplex * B, const int ldB,
                                        dcomplex * C, const int ldC) {
    char trans_A = 'N';
    char trans_B =  op_M_factor;   // 'T' - is transposed, 'N' - no modification, 'H' - hermite tr
    dcomplex alpha(1.0, 0.0);      //  C = alpha * A * B^T + beta * C
    dcomplex beta (0.0, 0.0);
    int Q_rows     = n_rows_A;     // rows of matrix Q
    int MhatX_cols = m_cols_B;     // columns of matrix C
    int Qc_MXr     = k_cA_rB;      // rows of matrix B == columns of matrix A
    int lda = ldA;
    int ldb = ldB;
    int ldc = ldC;

    zgemm_(&trans_A, &trans_B, &Q_rows, &MhatX_cols, &Qc_MXr,
                   &alpha, A, &lda, B, &ldb, &beta, C, &ldc);
}

///////////////////////////////////////////////////////////////////////////////

}  // End of namespace matrix_cross_2d

// End of the file

