/*
 * Copyright (c) 2010 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#include <iostream>
#include "mc2d_verbose_support.h"

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------

Verbose::Verbose() {
}

Verbose::~Verbose() {
}

//-----------------------------------------------------------------------------

bool Verbose::DebugMemory::showFlag_ = false;

Verbose::DebugMemory::DebugMemory() {
}

Verbose::DebugMemory::~DebugMemory() {
}

//static
void Verbose::DebugMemory::setShowFlag(bool flagExt) {
    showFlag_ = flagExt;
}

//static
bool Verbose::DebugMemory::show() {
    return showFlag_;
}

//-----------------------------------------------------------------------------

bool Verbose::UserInform::showFlag_ = false;

Verbose::UserInform::UserInform() {
}

Verbose::UserInform::~UserInform() {
}

//static
void Verbose::UserInform::setShowFlag(bool flagExt) {
    showFlag_ = flagExt;
}

//static
bool Verbose::UserInform::show() {
    return showFlag_;
}

//-----------------------------------------------------------------------------

bool Verbose::TechnicalDetails::showFlag_ = false;

Verbose::TechnicalDetails::TechnicalDetails() {

}

Verbose::TechnicalDetails::~TechnicalDetails() {
}

//static
void Verbose::TechnicalDetails::setShowFlag(bool flagExt) {
    showFlag_ = flagExt;
}

//static
bool Verbose::TechnicalDetails::show() {
    return showFlag_;
}

//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////

}  // End of the namespace matrix_cross_2d

// End of the file

