/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#ifndef  _TEMPLATE_MATRIX_UTILITIES_H_
#define  _TEMPLATE_MATRIX_UTILITIES_H_

#include <cstdlib>
#include <iostream>
#include "mc2d_defs_tcross.h"

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

///*! Fake conjugation for double type. Does not modifies the input at all. */
double conj(const double x);


/*! Return double(0) or complex<double>(0,0) */
template <typename T> T zero_init();


/*! Random fill a given matrix (a one-dimensional array)
 *  of size rows_sz x rank by random ints from
 *  the range [-MAX_MATRIX_VALUE, MAX_MATRIX_VALUE]. */
template <typename T>
void random_fill_in(T * const ptr_matrix, const int rows, const int cols);


/*! Copy first num_sz elements from A_src array to A_dist array. */
template <typename T>
void pure_copy(T * const A_src, T * const A_dist, const int num_sz);


/*! Find indeces of maximum submatrix. Input matrix A is used and modified.
 *  Works for double and std::complex<double> types only.  */
template <typename T>
void pure_maxvol(T * A, const int rows, const int cols, int * const Ind);


/*! Increase array capacity: deallocates the old memory,
 *  then allocates the new memory of size alloc_sz. */
template <typename T>
bool raw_reallocate(T * & Arr, const int alloc_sz, unique_ptr<T []> & X_ptr);


/*! Increase array capacity and keep first old_sz values of array.
 *  Copy values into a Buffer. Deallocates the memory.
 *  Allocates new memory of size alloc_sz. Copy elements from Buffer
 *  to the new allocated array.
 *  Data is saved in Fortran-like style (columnwise). */
template <typename T>
bool hist_reallocate(T * & Arr, const int old_sz, const int alloc_sz,
                     unique_ptr<T []> & X_ptr);


/*! Orthogonalize matrix A by applying QR decomposition.
 *  On output matrix Q from A = Q * R is saved in A. */
template <typename T>
bool qr_decomposition_Q(T * const A, const int nrows, const int kcols,
                        T * const buff_1, T * const buff_2, const int lwork_2);


 /*! Transpose a given matrix */
template <typename T>
void  transpose_square_matrix(T * const A, const int n_sz);


/*! Compute the Frobenius norm of the Ss array. */
double sqrt_sum2(double * const Ss, const int rank_K);


/*! Truncates the tail of Sigma. Sigma is supposed to be
 *  got from SVD array of singular values (> 0). Otherwise
 *  the result is undefined. */
int find_rank_from_sigma(double * const Sigma, const int rank, const double eps);


/*! Multiply the left or right orthogonal matrix Qu or Qv* by
 *  the square factor Mu (left) or Mv^T* (right). Is used
 *  after SVD truncation of the Mhat. */
template <typename T>
void Qx_mult_svd_factor_Mhatx(const char opB,
                              const int n_rows_A, const int m_cols_B, const int k_cA_rB,
                              T * A, const int ldA, T * B, const int ldB,
                              T * C, const int ldC);

///////////////////////////////////////////////////////////////////////////////

} // End of namespace matrix_cross_2d

#endif    // _TEMPLATE_MATRIX_UTILITIES_H_

// End of the file

