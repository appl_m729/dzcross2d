/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#ifndef _T_TARGET_FUNCTION_2D_H_
#define _T_TARGET_FUNCTION_2D_H_

#include "mc2d_defs_tcross.h"

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

/*! Class TargetFunction. Data for a given function to be approximated by the
 *  cross. Defines f(i,j). Parametrized class for double and complex<double>
 */
template <typename T> class TargetFunc2D {
public:
    TargetFunc2D();
    virtual ~TargetFunc2D();

    /*! Setup the number of mesh points. Rows. */
    bool  set_nx_rows(const int nRows);

    /*! Setup the number of mesh points. Columns. */
    bool  set_my_cols(const int mCols);

    /*! Matrix sizes.  */
    int  nx_rows() const;
    int  my_cols() const;

    /*! The value of the matrix element. */
    virtual T value(const int, const int) const;

    /*! Input correction test. */
    virtual bool are_bounds_setup_properly() const;

    /*! Setup auxiliary data 1.0/(n-1) to accelerate compuation
     *  of value(i,j). If not needed, just reemplement with
     *  empty body.  */
    virtual bool init();

private:
    int  nx_rows_;
    int  my_cols_;
};

///////////////////////////////////////////////////////////////////////////////

/*!  Interface for target function on the uniform
 *   two-dimensional rectangular mesh.
 *   Cartesian system is rotated clockwise 90 degrees.
 *   Oy axes goes from left to right, Ox axes goes from top to bottom.
 *   Defines mesh parameters and return value(i,j) */

class RectangularMesh2D {
public:
    RectangularMesh2D();
    virtual ~RectangularMesh2D();

    /*! Mesh bounds. */
    void   set_ax_top   (const double ax_t);
    void   set_bx_bottom(const double bx_b);
    void   set_ay_left  (const double ay_l);
    void   set_by_right (const double by_r);

    /*! Bounds of the definition rect. */
    double ax_top()    const;
    double bx_bottom() const;
    double ay_left()   const;
    double by_right()  const;

protected:
    double ax_top_;
    double bx_bottom_;
    double ay_left_;
    double by_right_;

    /*! Excess data to accelerate computation of function value. */
    double  one_over_nxm1_;
    double  one_over_mym1_;
    double  Dx_ab_;
    double  Dy_ab_;
};

/////////////////////////////////////////////////////////////////////////////////////

/*! Target function defined on the rectangular mesh. */
template <typename T>
class RectTargetFunc2D : public TargetFunc2D<T>,
                         public RectangularMesh2D {
public:
    RectTargetFunc2D();
    virtual ~RectTargetFunc2D();

    /*! Checks the input parameters.  */
    virtual bool are_bounds_setup_properly() const;

    /*! Setup additional parameters to hash some data. */
    virtual bool init() ;

    /*! Return value of the target function. Just default cap. */
    virtual T value(const int ix, const int jy) const;

    /*! Steps: x_(i+1) - x_(i), mesh is uniform (constant step) */
    double hx() const;
    double hy() const;
};

///////////////////////////////////////////////////////////////////////////////

}  // End of namespace matrix_cross_2d

#endif  //  _T_TARGET_FUNCTION_H_

// End of the file

