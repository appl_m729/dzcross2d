/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#ifndef _ADD_D_LAPACK_H_
#define _ADD_D_LAPACK_H_

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////
/*!
 * Computes the solution to a real system of linear equations
 *       A * X = B
 * where A is an n-by-n matrix and X and B are n-by-m matrices.
 * The LU decomposition with partial pivoting and row interchanges
 * is used to factor A as
 *       A = P * L * U
 * where P is a permutation matrix, L is unit lower triangular,
 * and U is upper triangular.  The factored form of A is then
 * used to solve the system of equations A * X = B
 */
extern "C" void dgesv_(int * n_rows, int * r_cols, double * A, int * a_lda,
                       int * Ipiv, double * B, int * ld_b, int * info);
/*!
 * n_rows - The number of linear equations (the order of
 *          the matrix A), n_rows >= 0
 * r_cols - The number of right hand sides (the number of columns of
 *          the matrix B)  r_cols >= 0.
 * A      - On entry, the n-by-n coefficient matrix A.
 *          On exit, the factors L and U from the factorization
 *          A = P*L*U; the unit diagonal elements of L are not stored.
 * a_lda  - The leading dimension of the array A,  a_lda >= max(1, n_rows)
 * Ipiv   - is int array, dimension n.
 *          The pivot indices that define the permutation matrix P
 *          row i of the matrix was interchanged with row Ipiv[i].
 * B      - is double array, dimension (ld_b, r_cols)
 *          On entry, the n-by-r matrix of right hand side matrix B.
 *          On exit, if info = 0, the n-by-r solution matrix X.
 * ld_b   - The leading dimension of the array B. ld_b >= max(1,n).
 *
 * info   - = 0:  successful exit
 *          < 0:  if info = -i, the i-th argument had an illegal value
 *          > 0:  if info =  i, U(i,i) is exactly zero.  The factorization
 *                has been completed, but the factor U is exactly
 *                singular, so the solution could not be computed.
 */

///////////////////////////////////////////////////////////////////////////////

/*! Copies elements from source to dist with increments. */
extern "C" void dcopy_(int * Nsz, double * source, int * xInc, double * dist, int * yInc);
// Nsz - size of the vector
// x - source vector
// y - distination vector
// xInc - increment of x
// yInc - increment of y

///////////////////////////////////////////////////////////////////////////////

/*! Computes Frobemious norm. */
extern "C" double dnrm2_(int * Nsz, double * array, int * inc);

///////////////////////////////////////////////////////////////////////////////

// A = P * L * U
extern "C" void dgetrf_(int * M_row, int * N_col, double * A, int * N_lda, int * Ipiv, int * inf);
// M_row - the number of rows
// N_col - the number of columns
// A     - matrix to factorize
// N_lda - the leading dimention
// IPIV  - INTEGER array, dimension (min(M,N))
//         The pivot indices; for 1 <= i <= min(M,N), row i of the
//         matrix was interchanged with row IPIV(i).
// inf   - information about errors; 0 - ok.

///////////////////////////////////////////////////////////////////////////////

/*!
 *  Solves A X = B or A^t X = B using output from dgetrf;
 */
extern "C" void dgetrs_(char * isTranspose, int * Nsz, int * Nrhs, double * A,
                        int * N_lda, int * Ipiv, double * B, int * B_lda, int * info);

///////////////////////////////////////////////////////////////////////////////

/*! Finds maximum element in the array Array of size Nsize.
 *  It goes throw the array from a[0] to a[Nsz-1] with
 *  the step equal to Increment.
 */
extern "C" int idamax_(int * Nsize, double * Array, int * Increment);

///////////////////////////////////////////////////////////////////////////////

/*! A_matrix = alpha * x_vector * y_vector^T + A
 *  Size of the vector x_vector is x_size.
 *  Size of the vector y_vector is y_size.
 *  Size of the A_matrix is x_size * y_size.
 *  alpha - scalar.
 */
extern "C" void dger_(int * x_size, int * y_size, double * alpha,
                      double * x_vector, int * increment_x,
                      double * y_vector, int * increment_y,
                      double * A_matrix, int * n_lda);

///////////////////////////////////////////////////////////////////////////////
/*!
 * This function computes the singular value decomposition (SVD) of a real
 * n-by-r matrix A, optionally computing the left and/or right singular
 * vectors. The SVD is written as A = U * SIGMA * transpose(V)
 * where SIGMA is an n-by-r matrix which is zero except for its
 * min(n,r) diagonal elements, U is an n-by-n orthogonal matrix, and
 * V is an r-by-r orthogonal matrix. The diagonal elements of SIGMA
 * are the singular values of A; they are real and non-negative, and
 * are returned in descending order. The first min(n,r) columns of
 * U and V are the left and right singular vectors of A.
 * Note that the routine returns Vt, not V.
 */
extern "C" void dgesvd_(char * job_U_in, char * job_Vt_in,
                        int * n_rows_in, int * r_cols_in,
                        double * A_matrix_in_out, int * A_leading_dim_in,
                        double * S_out, double * U_out, int * ld_U_in,
                        double * Vt_out, int * ld_Vt_in,
                        double * work_in_out, int * l_work_in, int * info_out);

/*! job_U_in - Specifies options for computing all or part of the matrix U.
 *           = 'A':  all n columns of U are returned in array U.
 *           = 'S':  the first min(n,r) columns of U (the left singular vectors)
 *                   are returned in the array U.
 *           = 'O':  the first min(n,r) columns of U (the left singular vectors)
 *                   are overwritten on the array A;
 *           = 'N':  no columns of U (no left singular vectors) are computed.
 *
 *  job_Vt_in - Specifies options for computing all or part of the matrix Vt.
 *            = 'A':  all r rows of Vt are returned in the array Vt;
 *            = 'S':  the first min(n,r) rows of Vt (the right singular vectors)
 *                    are returned in the array Vt;
 *            = 'O':  the first min(n,r) rows of Vt (the right singular vectors)
 *                    are overwritten on the array A;
 *            = 'N':  no rows of Vt (no right singular vectors) are computed.
 *
 *  job_U_in and job_Vt_in cannot both be 'O'.
 *
 *  n_rows_in - The number of rows of the input matrix A, n >= 0.
 *  r_cols_in - The number of columns of the input matrix A, r >= 0.
 *
 *  A_matrix_in_out  -  (input/output) double-type array, dimension (A_leading_dim, r)
 *                      On entry, the n-by-r matrix A. On exit
 *  if job_U_in = 'O',  A is overwritten with the first min(n,r)
 *                      columns of U (the left singular vectors, stored columnwise);
 *  if job_Vt_in= 'O',  A is overwritten with the first min(n,r)
 *                      rows of Vt (the right singular vectors, stored rowwise);
 *  if job_U_in .ne. 'O' and job_Vt_in .ne. 'O', the contents of A are destroyed.
 *
 *  A_leading_dim_in - The leading dimension of the input array A.
 *                     >= max(1,n).
 *
 *  S_out - double array, dimension min(n,r). The singular values of A,
 *          sorted so that S[i] >= S[i+1].
 *
 *  U_out - double array, dimension
 *          (ld_U_in, n) if job_U_in = 'A' or (ld_U_in, min(n,r)) if job_U_in = 'S'.
 *          If job_U_in = 'A', U_out contains the n-by-n orthogonal matrix U_out.
 *          If job_U_in = 'S', U-out contains the first min(n,r) columns of U_out
 *          (the left singular vectors, stored columnwise).
 *          If job_U_in = 'N' or 'O', U_out is not referenced.
 *
 *  ld_U_in - The leading dimension of the array U_out.
 *            It is >= 1. If job_U_in = 'S' or 'A', ld_U_in >= n.
*
*  VT      (output) DOUBLE PRECISION array, dimension (LDVT,N)
*          If JOBVT = 'A', VT contains the N-by-N orthogonal matrix
*          V**T;
*          if JOBVT = 'S', VT contains the first min(m,n) rows of
*          V**T (the right singular vectors, stored rowwise);
*          if JOBVT = 'N' or 'O', VT is not referenced.
*
*  LDVT    (input) INTEGER
*          The leading dimension of the array VT.  LDVT >= 1; if
*          JOBVT = 'A', LDVT >= N; if JOBVT = 'S', LDVT >= min(M,N).
*
*  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
*          On exit, if INFO = 0, WORK(1) returns the optimal LWORK;
*          if INFO > 0, WORK(2:MIN(M,N)) contains the unconverged
*          superdiagonal elements of an upper bidiagonal matrix B
*          whose diagonal is in S (not necessarily sorted). B
*          satisfies A = U * B * VT, so it has the same singular values
*          as A, and singular vectors related by U and VT.
*
*  LWORK   (input) INTEGER
*          The dimension of the array WORK.
*          LWORK >= MAX(1,3*MIN(M,N)+MAX(M,N),5*MIN(M,N)).
*          For good performance, LWORK should generally be larger.
*
*          If LWORK = -1, then a workspace query is assumed; the routine
*          only calculates the optimal size of the WORK array, returns
*          this value as the first entry of the WORK array, and no error
*          message related to LWORK is issued by XERBLA.

 *
 *  info_out - If it is =  0, successful exit.
 *                      = -i, the i-th argument had an illegal value.
 *                      >  0, the DBDSQR-routine did not converge,
 *             info_out specifies how many superdiagonals of an intermediate
 *             bidiagonal form B did not converge to zero. See the description
 *             of WORK above for details.
 */
///////////////////////////////////////////////////////////////////////////////

/*! QR decomposition of a given matrix A without pivoting.
 *  On entry, the n-by-r matri  x A.
 *  On exit, the elements on and above the diagonal of the array
 *  contain the min(n,r)-by-r upper trapezoidal matrix R
 *  (R is upper triangular if n >= r).
 *  The elements below the diagonal, with the array tau, represent
 *  the orthogonal matrix Q.
 *  The matrix Q is represented as a product of elementary reflectors
 *  Q = H(1) H(2) ... H(k), where k = min(n,r).
 *  Each H(i) has the form H(i) = I - tau * v * v^T
 *  where tau is a real scalar, and v is a real vector with
 *  v(1:i-1) = 0 and v(i) = 1; v(i+1:n) is stored on exit in A(i+1:n,i),
 *  and tau in tau(i).
 */
extern "C" void dgeqrf_(int * rows_number_n_in, int * cols_number_r_in,
                        double * A_in_out, int * leading_dimension_in,
                        double * tau_out, double * work_out,
                        int * l_work_in, int * info_out);

/*! rows_number_n_in     - The number of rows in input matrix, input parameter.
 *  cols_number_r_in     - The number of cols in input matrix, input parameter.
 *  A_in_out             - The matrix, input and output.
 *  leading_dimension_in - The leading dimension, input.
 *  tau_out              - The array of scalars in Hausholder reflections, output.
 *  work_out             - The double array, dimension max(1,l_work_in).
 *                         On exit, if info_out == 0, l_wokr_in[0] returns
 *                         the optimal work_out.
 *  l_work_in            - The dimension of the array work_out. l_work_in >= max(1,N).
 *                         For optimum performance l_work_in >= N*NB, where NB is
 *                         the optimal blocksize.
 *                         If l_work_in = -1, then a workspace query is assumed;
 *                         the routine only calculates the optimal size of the work_out
 *                         array, returns this value as the first entry of the work_out
 *                         array, and no error message related to l_work_in is issued by XERBLA.
 *  info_out             - Integer out. If it = 0, then successful exit,
 *                         if it = -i, the i-th argument had an illegal value.
 */

///////////////////////////////////////////////////////////////////////////////

/*! This function generates an n-by-r real matrix Q with orthonormal columns,
 *  which is defined as the first r columns of a product of k elementary
 *  reflectors of order n.
 *  Q  =  H(1)H(2)...H(k)  as returned by DGEQRF.
 *
 *  reflectors_number_k - Input parameter. The number of elementary
 *                        reflectors whose product defines the matrix Q.
 *                        r >= k >= 0.
 *  A_in_out - The matrix of dimension n-by-r, input and output.
 *             On entry, the i-th column must contain the vector which
 *             defines the elementary reflector H(i), for i = 1,2,...,k, as
 *             returned by DGEQRF in the first k columns of its
 *             array argument A. On exit, the n-by-r matrix Q.
 */
extern "C" void dorgqr_(int * rows_number_n_in, int * cols_number_r_in,
                        int * reflectors_number_k,
                        double * A_in_out, int * leading_dimension_in,
                        double * tau_out, double * work_out,
                        int * l_work_in, int * info_out);

///////////////////////////////////////////////////////////////////////////////

/*!  Performs one of the matrix-matrix operations
 *   C := alpha * op(A) * op(B) + beta * C
 *   where  op(X) is one of op(X) = X or op(X) = X^T.
 *   alpha and beta are scalars,
 *   A is M x K matrix, B is K x N matrix, C is M x N matrix.
 */
extern "C" void dgemm_(char * trans_A_in, char * trans_B_in, int * m_rows_AC_in,
                       int * n_cols_BC_in, int * k_cols_A_rows_B_in, double * alpha_in,
                       double * A_in, int * ld_A_in, double * B_in, int * ld_B_in,
                       double * beta, double * C_in_out, int * ld_C_in);

/*! trans_A_in = 'N' or 'n', if op(A) = A.
 *             = 'T' or 't', if op(A) = A^T.
 *             = 'C' or 'c', if op(A) = conjugate(A).
 *  trans_B_in - the same but for B.
 *  m_rows_in  - the number of rows of the matrix op(A) and of the matrix C.
 *  n_cols_in  - the number of cols of the matrix op(B) and of the matrix C.
 *  k_cols_A_rows_B_in - the number of cols of the matrix op(A) and
 *                       the number of rows of the matrix op(B).
 */
///////////////////////////////////////////////////////////////////////////////

}  // End of the namespace matrix_cross_2d

#endif  // _ADD_D_LAPACK_H_

// End of the file

