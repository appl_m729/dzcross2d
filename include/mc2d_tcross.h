/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#ifndef _TEMPLATE_CROSS_2D_H_
#define _TEMPLATE_CROSS_2D_H_

#include "mc2d_defs_tcross.h"

namespace matrix_cross_2d {

// Forward declaration
template <typename> class TargetFunc2D;

///////////////////////////////////////////////////////////////////////////////

/*! The cross approximation of a two-dimensional function.
 *
 *                A = U * Sigma * transposed(V)
 *
 *  Where U and V are supposed to be of low-rank format.
 *  It is implemented for double and complex<double> types.
 *  Interface class. Contains only those data which are needed
 *  to provide the decomposition. */

template <typename T>
class BaseCross2D {
public:
    BaseCross2D();
    virtual ~BaseCross2D();

    BaseCross2D(const BaseCross2D * ) = delete;
    BaseCross2D(BaseCross2D && ) = delete;
    BaseCross2D & operator = (const BaseCross2D & ) = delete;
    BaseCross2D & operator = (BaseCross2D &&) = delete;

    /*! Free the allocated memory before the next approximation, if needed. */
    virtual void  release_base_memory();

    /*! Deallocates buffers used in decomposition algorithms
     *  in inherited classes. */
    virtual void  release_auxiliary_memory() = 0;

    /*! Pointer to the target function to be approximated.
     *  Call this func before the set_guess_matrix_U or
     *  set_guess_matrix_V. */
    bool  set_target_function(TargetFunc2D<T> * const targetFunc);

    /*! Starting rank (can be reduced/encreased then.)
     *  Call this function before the set_guess_matrix_U or
     *  set_guess_matrix_V. */
    bool  set_guess_rank(const int guessRank);

    /*! Starting approximation for r (rank) left unitary vectors
     *  for the decomposition  A = U * Sigma * V^H.
     *  If skipped, then starting from random.
     *  The number of rows is defined by target function rows.
     *  The number of columns is set by the guess rank. */
    virtual bool  set_guess_matrix_U(T * const U_adress) = 0;

    /*! Starting approximation for r (rank) right unitary vectors
     *  for decomposition  A = U * Sigma * V^t.
     *  If skipped, then starting from random.
     *  The number of rows is defined by target function cols.
     *  The number of columns is set by the guess rank. */
    virtual bool  set_guess_matrix_V(T * const V_adress) = 0;

    /*! When compute the approximation, the exact matrix
     *  is in fact larger (we suppose it is of low rank format).
     *  We truncate its singulars by this relative epsilon error.  */
    void  set_truncation_accuracy(const double rtEpsilon);

    /*! Manual main loop control. */
    virtual void  set_iter_limit(const int iterLimit) = 0;

    /*! Number of rows in matrix or the number of mesh points in (first) x-dimension. */
    int  nx_rows() const;

    /*! Number of columns in matrix or the number of mesh points in (second) y-dimension. */
    int  my_cols() const;

    /*! Main solver (low-rank, svd, etc.). */
    bool  do_decompose();

    /*! Matrix U. Fortran-like order. */
    const T * adress_U() const;

    /*! Matrix V is Hermitian conjugated. Fortran like order. */
    const T * adress_V() const;

    /*! Releases ownership of U stored pointer,
     * by returning its value and replacing it with a null pointer  */
    T * release_U();

    /*! Releases ownership of Vh stored pointer,
     * by returning its value and replacing it with a null pointer  */
    T * release_V();

    /*! Releases ownership of Sigma stored pointer,
     * by returning its value and replacing it with a null pointer  */
    double * release_Sigma();

    /*! Trancation accuracy (relates to singular values). */
    double accuracy() const;

    /*! Compute A(i,j) = sum_over_k[ U(i,k) * Sigma(k) * V^T(k,j) ] */
    T approx_value(const int i, const int j) const;

    /*!  A = U * Sigma * V^T;  r (rank) left orthogonal vectors. */
    T  U(const int i, const int k) const;

    /*!  A = U * Sigma * V^T;  r (rank) singular values. */
    double  Sigma(const int k) const;

    /*!  A = U * Sigma * V^t;  r (rank) right transposed orthogonal vectors. */
    T  V(const int j, const int k) const; // h - Hermitian conj

    /*! Actual epsilon-rank of the decomposition. */
    int rank() const;

    /*! Is just a starting rank supposed by user. */
    int guess_rank() const;

    /*! ...  */
    T* matvec(T *vector, char option) const;

protected:
    /*! Provides double f(i,j) and other parameters of mesh. */
    TargetFunc2D<T> * generator_fm_;

    /*! Initial rank  */
    bool  is_guess_rank_setup_;

    /*! Current rank. This is the actual rank of the decomposition.  */
    int      rank_;
    /*! Just a guess rank, to start with it. */
    int      guess_rank_;
    /*! Cross decomposition accuracy */
    double   rank_trancation_epsilon_;

    /*! Basis columns. They are passed into lapack directly. */
    T * Ur_;
     /*! Basis rows. */
    T * Vr_;

    /*! Smart pointers which properly deallocates memory for Ur_, Vr_ arrays. */
    unique_ptr<T []>  up_Ur_;
    unique_ptr<T []>  up_Vr_;

    /*! Singular values. */
    double * Sigma_;
    upDblArr up_Sigma_;

private:
    /*! Extension-contraction cross, SVD, Schur complement cross. */
    virtual bool do_decomposition_method_() = 0;
};

///////////////////////////////////////////////////////////////////////////////

/*! Apply Singular Value Decomposition to the matrix A
 *  and truncate the singular values then. */

template <typename T>
class SVDCross2D : public BaseCross2D<T> {
public:
    SVDCross2D();
    virtual ~SVDCross2D();

    SVDCross2D(const SVDCross2D & ) = delete;
    SVDCross2D(SVDCross2D && ) = delete;

    SVDCross2D & operator = (const SVDCross2D & ) = delete;
    SVDCross2D & operator = (SVDCross2D && ) = delete;

    virtual void release_base_memory();
    virtual void release_auxiliary_memory();

    virtual bool set_guess_matrix_U(T * const);
    virtual bool set_guess_matrix_V(T * const);

    virtual void set_iter_limit(const int);
private:
    /*! Left  singular vectors. */
    T *      Usvd_;
    /*! Right singular vectors. */
    T *      Vsvd_;
    /*! Singular values. */
    double * Ssvd_;
    /*! Full matrix to be diagonalized. */
    T *      Asvd_;
    unique_ptr<T []>      up_Usvd_;
    unique_ptr<T []>      up_Vsvd_;
    unique_ptr<double []> up_Ssvd_;
    unique_ptr<T []>      up_Asvd_;

    /*! Buffers data for SVD Lapack */
    T * Work_;
    unique_ptr<T []> up_Work_;
    double   * Rwork_;
    unique_ptr<double []> up_Rwork_;

    /*! Sizes for allocated memory */
    int  alloc_lwork_;
    int  alloc_rows_;
    int  alloc_cols_;
    int  alloc_rank_;
    int  alloc_n_r_;
    int  alloc_r_m_;
    int  alloc_sr_;

    /*! The main decomposition routine. */
    virtual bool do_decomposition_method_() ;

    /*! Memory reallocation */
    void  realloc_svd_arrays_();

    /*! BaseCross2D data. */
    void  reallocate_base_arrays_();

    /*! Fill up matrix A by f(i,j). */
    void compute_full_matrix_A_();

    /*! Lapack SVD decomposition. */
    bool direct_svd_of_A_();

    /*! Truncate singular values. */
    void truncate_svd_Sigma_();

    /*! Copy data from svd arrays ti BaseCross2D arrays. */
    void bind_interface_matrices_();
};

///////////////////////////////////////////////////////////////////////////////

/*! Extension-(contraction at the end) cross. At each step it increases the current
 *  rank by a factor of 2. Then reduces the rank-subspace by svd
 *  truncation and repeat. */

template <typename T>
class StableCross2D : public BaseCross2D<T> {
public:
    StableCross2D();
    virtual ~StableCross2D();

    StableCross2D(const StableCross2D & ) = delete;
    StableCross2D(StableCross2D && ) = delete;

    StableCross2D & operator = (const StableCross2D & ) = delete;
    StableCross2D & operator = (StableCross2D && ) = delete;

    virtual void release_base_memory();
    virtual void release_auxiliary_memory();

    virtual bool set_guess_matrix_U(T * const);
    virtual bool set_guess_matrix_V(T * const);

    virtual void set_iter_limit(const int iterLimit);
private:

    /*! Start approximation from input. */
    bool  is_Ur_setup_;
    bool  is_Vr_setup_;

    /*! Maximum size of rank for which there is enough allocated memory.
     *  for base matrices. When rank_ > base_alloc_rank_
     *  the memory for these matrices is reallocated (with
     *  history copying if needed). */
    int  base_alloc_rank_;

    /*! Maximum size of rank for which there is enough allocated memory.
     *  for auxiliary arrays. When rank_ > auxiliary_alloc_rank_
     *  the memory for these matrices is reallocated (with
     *  history copying if needed). */
    int  auxiliary_alloc_rank_;

    /*! Size of the working array (see Lapack QR and SVD)  */
    int  lwork_size_x_;

    /*! Maximum value of the cross-2d iterations. If achieved then failed. */
    int  iterLimit_;

    /*! Maxvol destroys input array. So, buffers are needed. */
    T * Ur_copy_;
    T * Vr_copy_;
    unique_ptr<T []> up_Ur_copy_;
    unique_ptr<T []> up_Vr_copy_;
    /*! maxvol row indeces for columns */
    int * I_u_;
    /*! maxvol column indeces for rows */
    int * I_v_;
    upIntArr up_I_u_;
    upIntArr up_I_v_;

    /*! Auxiliary buffer: used in copy_with_history, QR, SVD */
    T * buffer_;
    unique_ptr<T []> up_buffer_;
    /*! Singular values buffer for a small svd routine. */
    double * sd_buff_;
    unique_ptr<double []> up_sd_buff_;

    /*! Hats (the matrices from maxvol) */
    T * A_hat_;
    T * Qu_hat_;
    T * Qv_hat_;
    unique_ptr<T []> up_A_hat_;
    unique_ptr<T []> up_Qu_hat_;
    unique_ptr<T []> up_Qv_hat_;

    /*! Convergence criterion by approximant.
     *  Singular values from the previous iteration.*/
    double * old_singular_sgm_;
    /*! Singular values. */
    upDblArr up_old_singular_sgm_;
    /*! Ranks of the small approximant matrices.*/
    int  old_M_rank_;
    int  M_rank_;


    /*! Check that before init of U and V the target function,
     *  guess_rank and memory are setup and allocated properly. */
    bool preliminarily_init_alloc_();

    /*! Main decomposition method. */
    virtual bool do_decomposition_method_();

    /*! Initial approximation. */
    bool setup_from_input_otherwise_random_();

    void drop_setup_flags_();

    /*! Memory reallocation routines if rank exceeds memory allocated size. */
    bool allocate_extra_memory_(const int size_new);
    bool allocate_base_extra_memory_(const int size_new);
    bool allocate_auxiliary_extra_memory_(const int size_new);

    /*! Base arrays memory reallocation. */
    bool reallocate_base_memory_(const int old_rank, const int rank_new);

    /*! Auxiliary arrays memory reallocation. */
    bool reallocate_auxiliary_memory_(const int old_rank, const int rank_new);

    /*! Recalculates the allocated rank memory size. */
    void increse_allocated_rank_(int & inp_alloc_rank, const int size_new);

    /*! Compute elements of f(:,I) for new columns of Ur. */
    void maxvol_complement_Ur_x2_(T * const Uq, const int rows_sz,
                                          const int * const Ind_v);

    /*! Compute elements of f(I,:) for new columns of Vr. */
    void maxvol_complement_Vr_x2_(T * const Vq, const int cols_sz,
                                          const int * const Ind_u);

    /*! Find kernel M (inverse A_hat): A hat construction. */
    void create_A_hat_();

    /*! Extract Q-hat from the Q matrix. */
    void extract_Q_hat_(T * const Qx_hat, const T * const Qx_full,
                         const int row_sz, const int * const Ix);

    /*! Finds M_hat in two steps. */
    bool solve_M_hat_();

    /*! Decompose kernel M by SVD. */
    bool svd_M_approximant_();

    /*! Prints out svd-values for the snall matrix Mhat. For debug. */
    void print_singulars_(const int K_rank, const int old_k_rank);

    /*! Estimate diffrence between old and current singular values. */
    double F_norm_sigma_error_(const int old_rank_K, const int rank_K) const;

    /*! If a given matrix is of (almost) full rank.
     *  At least 2 * current_rank > min(nx_rows, my_cols). */
    bool  full_rank_matrix_svd_();
};

///////////////////////////////////////////////////////////////////////////////

}  // End of namespace matrix_cross_2d

#endif  // _TEMPLATE_CROSS_2D_H_

// End of the file

