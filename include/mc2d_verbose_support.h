/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#ifndef _VERBOSE_SUPPORT_H_
#define _VERBOSE_SUPPORT_H_

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

class Verbose final {
public:
    Verbose();
    ~Verbose();

    Verbose(const Verbose & ) = delete;
    Verbose(Verbose && ) = delete;
    Verbose & operator = (const Verbose & ) = delete;
    Verbose & operator = (Verbose && ) = delete;

//-----------------------------------------------------------------------------
    class DebugMemory final {
    public:
        DebugMemory();
        ~DebugMemory();

        DebugMemory(const DebugMemory & ) = delete;
        DebugMemory(DebugMemory && ) = delete;
        DebugMemory & operator = (const DebugMemory & ) = delete;
        DebugMemory & operator = (DebugMemory && ) = delete;

        static void setShowFlag(bool flag = true);
        static bool show();

    private:
        static bool showFlag_;
    };
//-----------------------------------------------------------------------------
    class UserInform final {
    public:
        UserInform();
        ~UserInform();

        UserInform(const UserInform & ) = delete;
        UserInform(UserInform && ) = delete;
        UserInform & operator = (const UserInform & ) = delete;
        UserInform & operator = (UserInform && ) = delete;

        static void setShowFlag(bool flag = true);
        static bool show();

    private:
        static bool showFlag_;
    };
//-----------------------------------------------------------------------------
    class TechnicalDetails final {
    public:
        TechnicalDetails();
        ~TechnicalDetails();

        TechnicalDetails(const TechnicalDetails & ) = delete;
        TechnicalDetails(TechnicalDetails &&) = delete;
        TechnicalDetails & operator = (const TechnicalDetails & ) = delete;
        TechnicalDetails & operator = (TechnicalDetails && ) = delete;

        static void setShowFlag(bool flag = true);
        static bool show();

    private:
        static bool showFlag_;
    };
};

///////////////////////////////////////////////////////////////////////////////

}  // End of the namespace  matrix_cross_2d

#endif   // _VERBOSE_SUPPORT_H_

// End of the file

