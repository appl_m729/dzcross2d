/*
 * Copyright (c) 2015 Mikhail Litsarev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU LGPL for more details.
 */

#ifndef _CROSS_2D_DEFINITIONS_H_
#define _CROSS_2D_DEFINITIONS_H_

#include <memory>
#include <complex>

namespace matrix_cross_2d {

///////////////////////////////////////////////////////////////////////////////

using std::unique_ptr;

/*! For randomization of the initial approximation. */
const int MAX_MATRIX_VALUE = 1024;

/*! Complex value type */
typedef std::complex<double> dcomplex;

/*! Define array types allocated in smart pointers. */
typedef  unique_ptr<int [] >      upIntArr;
typedef  unique_ptr<double [] >   upDblArr;
typedef  unique_ptr<dcomplex [] > upCmplxArr;

/*! Maxvol subroutine.
 *  \param A_in     - input matrix of size n x r.
 *  \param Ind_out  - array of maxvol indeces of size r.
 *  \param nswp_in  - maximum number of swapping iterations.
 *  \param tol_in   - accuracy threshold. */
extern "C" void dmaxvol_(double * A_in, int * n_in, int * r_in,
                         int * Ind_out, int * nswp_in, double * tol_in);

extern "C" void zmaxvol_(dcomplex * A_in, int * n_in, int * r_in,
                         int * Ind_out, int * nswp_in, double * tol_in);

///////////////////////////////////////////////////////////////////////////////

} // End of namespace matrix_cross_2d

#endif   // _CROSS_2D_DEFINITIONS_H_

// End of the file

