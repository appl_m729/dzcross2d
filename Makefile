include  ./settings/Makefile.in

all:
	(cd ./src && make && make clean)
	(cd ./examples && make && make clean)
lib:    
	(cd ./src && make && make clean)
clean:
	(cd ./src && make clean)
	(cd ./examples && make clean )

cleanlib:
	rm -f ./libs/lib$(LIB_CROSS_UTILS).a


# End of file

